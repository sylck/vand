#! /bin/bash

file=bin/activate
if [[ -f "$file" ]]; then
    echo "'$file' exists. Setup already performed. Aborting."
    exit 1
fi

virtualenv -p python3 .
source bin/activate
pip install -r requirements.txt

python src/manage.py migrate

echo "Creating administrator user account..."
python src/manage.py createsuperuser

chmod +x run.sh
