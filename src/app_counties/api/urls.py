from django.urls import path

from .views import (
    CountyIndexStoreAPIView,
    CountyUpdateDestroyAPIView,
)


app_name = 'counties'

urlpatterns = [
    path('',          CountyIndexStoreAPIView.as_view(),    name='index-store'),     # /api/counties/
    path('<int:pk>/', CountyUpdateDestroyAPIView.as_view(), name='update-destroy'),  # /api/counties/42/
]
