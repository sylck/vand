from rest_framework import generics, permissions
from rest_framework.authentication import TokenAuthentication
from rest_framework.exceptions import PermissionDenied

from vand.api.views import BaseManageView

from app_users.models import UserRole

from .serializers import CountyModelSerializer
from ..models import County


class CountyStoreAPIView(generics.CreateAPIView):
    authentication_classes = [TokenAuthentication, ]
    permission_classes = [permissions.IsAuthenticated, ]
    serializer_class = CountyModelSerializer

    def perform_create(self, serializer):
        if self.request.user.role != UserRole.Admin.value:
            raise PermissionDenied(detail='Must be administrator to perform this action.')

        serializer.save()


class CountyIndexAPIView(generics.ListAPIView):
    queryset = County.objects.all()
    serializer_class = CountyModelSerializer


class CountyIndexStoreAPIView(BaseManageView):
    VIEWS_BY_METHOD = {
        'POST': CountyStoreAPIView.as_view,
        'GET': CountyIndexAPIView.as_view,
    }


class CountyUpdateAPIView(generics.UpdateAPIView):
    authentication_classes = [TokenAuthentication, ]
    permission_classes = [permissions.IsAuthenticated, ]
    queryset = County.objects.all()
    serializer_class = CountyModelSerializer

    def perform_update(self, serializer):
        if self.request.user.role != UserRole.Admin.value:
            raise PermissionDenied(detail='Must be administrator to perform this action.')

        serializer.save()


class CountyDestroyAPIView(generics.DestroyAPIView):
    authentication_classes = [TokenAuthentication, ]
    permission_classes = [permissions.IsAuthenticated, ]
    queryset = County.objects.all()

    def perform_destroy(self, instance):
        if self.request.user.role != UserRole.Admin.value:
            raise PermissionDenied(detail='Must be administrator to perform this action.')

        instance.delete()


class CountyUpdateDestroyAPIView(BaseManageView):
    VIEWS_BY_METHOD = {
        'PUT': CountyUpdateAPIView.as_view,
        'DELETE': CountyDestroyAPIView.as_view,
    }
