from rest_framework import serializers

from ..models import County


class CountyModelSerializer(serializers.ModelSerializer):
    class Meta:
        model = County
        fields = ['id', 'name']
