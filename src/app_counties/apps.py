from django.apps import AppConfig


class AppCountiesConfig(AppConfig):
    name = 'app_counties'
