from django.db import models


class County(models.Model):
    name = models.CharField(max_length=50, unique=True)

    def __str__(self):
        return str(self.name)

    class Meta:
        ordering = ('name', )
        verbose_name_plural = "counties"
