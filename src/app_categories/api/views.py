from rest_framework import generics, permissions
from rest_framework.authentication import TokenAuthentication
from rest_framework.exceptions import PermissionDenied

from vand.api.views import BaseManageView

from app_users.models import UserRole

from .serializers import CategoryModelSerializer
from ..models import Category


class CategoryStoreAPIView(generics.CreateAPIView):
    authentication_classes = [TokenAuthentication, ]
    permission_classes = [permissions.IsAuthenticated, ]
    serializer_class = CategoryModelSerializer

    def perform_create(self, serializer):
        if self.request.user.role != UserRole.Admin.value:
            raise PermissionDenied(detail='Must be administrator to perform this action.')

        serializer.save()


class CategoryIndexAPIView(generics.ListAPIView):
    queryset = Category.objects.all()
    serializer_class = CategoryModelSerializer


class CategoryIndexStoreAPIView(BaseManageView):
    VIEWS_BY_METHOD = {
        'POST': CategoryStoreAPIView.as_view,
        'GET': CategoryIndexAPIView.as_view,
    }


class CategoryUpdateAPIView(generics.UpdateAPIView):
    authentication_classes = [TokenAuthentication, ]
    permission_classes = [permissions.IsAuthenticated, ]
    queryset = Category.objects.all()
    serializer_class = CategoryModelSerializer

    def perform_update(self, serializer):
        if self.request.user.role != UserRole.Admin.value:
            raise PermissionDenied(detail='Must be administrator to perform this action.')

        serializer.save()


class CategoryDestroyAPIView(generics.DestroyAPIView):
    authentication_classes = [TokenAuthentication, ]
    permission_classes = [permissions.IsAuthenticated, ]
    queryset = Category.objects.all()

    def perform_destroy(self, instance):
        if self.request.user.role != UserRole.Admin.value:
            raise PermissionDenied(detail='Must be administrator to perform this action.')

        instance.delete()


class CategoryUpdateDestroyAPIView(BaseManageView):
    VIEWS_BY_METHOD = {
        'PUT': CategoryUpdateAPIView.as_view,
        'DELETE': CategoryDestroyAPIView.as_view,
    }
