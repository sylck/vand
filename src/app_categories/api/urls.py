from django.urls import path

from .views import (
    CategoryIndexStoreAPIView,
    CategoryUpdateDestroyAPIView,
)


app_name = 'categories'

urlpatterns = [
    path('',          CategoryIndexStoreAPIView.as_view(),    name='index-store'),     # /api/categories/
    path('<int:pk>/', CategoryUpdateDestroyAPIView.as_view(), name='update-destroy'),  # /api/categories/42/
]
