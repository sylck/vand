from django.db import models


class Category(models.Model):
    title = models.CharField(max_length=50, unique=True)

    def __str__(self):
        return str(self.title)

    class Meta:
        ordering = ('title', )
        verbose_name_plural = "categories"
