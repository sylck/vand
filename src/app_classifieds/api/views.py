from django.db.models import Q

from rest_framework import generics, permissions
from rest_framework.authentication import TokenAuthentication
from rest_framework.exceptions import ValidationError, PermissionDenied, NotFound
from rest_framework.response import Response
from rest_framework.views import APIView

from vand.api.views import BaseManageView

from app_users.models import UserRole

from .serializers import ClassifiedModelSerializer
from ..models import Classified


class ClassifiedStoreAPIView(APIView):
    authentication_classes = [TokenAuthentication, ]
    permission_classes = [permissions.IsAuthenticated, ]

    def post(self, request, format=None):
        title = request.POST.get('title')
        description = request.POST.get('description')
        price = request.POST.get('price')
        category_id = request.POST.get('category')
        image_ids = request.POST.getlist('images[]')

        if not all([title, description, price, category_id, image_ids]):
            raise ValidationError(detail='Incomplete data provided.')

        try:
            price = int(price)
        except Exception:
            raise ValidationError(detail='Price not valid.')

        try:
            category_id = int(category_id)
        except Exception:
            raise ValidationError(detail='Category ID not valid.')

        try:
            for i, image_id in enumerate(image_ids):
                image_ids[i] = int(image_id)
        except Exception:
            raise ValidationError(detail='Image IDs not valid.')

        classified = Classified.objects.create(
            title=title, description=description, price=price, category_id=category_id, seller=request.user
        )
        classified.images.add(*image_ids)

        content = ClassifiedModelSerializer(classified).data
        return Response(content)


class ClassifiedIndexAPIView(generics.ListAPIView):
    queryset = Classified.objects.all()
    serializer_class = ClassifiedModelSerializer

    def get_queryset(self, *args, **kwargs):
        qs = self.queryset
        filters = Q()

        query = self.request.GET.get('q')
        category_id = self.request.GET.get('category')
        county_id = self.request.GET.get('county')
        seller_id = self.request.GET.get('user')

        try:
            category_id = int(category_id)
        except Exception:
            category_id = None

        try:
            county_id = int(county_id)
        except Exception:
            county_id = None

        try:
            seller_id = int(seller_id)
        except Exception:
            seller_id = None

        if query is not None:
            query_filter = Q(title__icontains=query) | Q(description__icontains=query)
            filters &= query_filter

        if category_id is not None:
            filters &= Q(category=category_id)

        if county_id is not None:
            filters &= Q(seller__county=county_id)

        if seller_id is not None:
            filters &= Q(seller=seller_id)

        qs = qs.filter(filters)
        qs = qs.order_by('-date_posted')

        return qs


class ClassifiedIndexStoreAPIView(BaseManageView):
    VIEWS_BY_METHOD = {
        'POST': ClassifiedStoreAPIView.as_view,
        'GET': ClassifiedIndexAPIView.as_view,
    }


class ClassifiedUpdateAPIView(APIView):
    authentication_classes = [TokenAuthentication, ]
    permission_classes = [permissions.IsAuthenticated, ]

    def put(self, request, pk, format=None):
        classified_id = pk

        title = request.POST.get('title')
        description = request.POST.get('description')
        price = request.POST.get('price')
        category_id = request.POST.get('category')
        image_ids = request.POST.getlist('images[]')

        classified_objs = Classified.objects.filter(pk=classified_id)

        if not classified_objs.exists():
            raise NotFound(detail='Classified with given ID does not exist.')

        classified = classified_objs.first()

        if request.user != classified.seller and request.user.role != UserRole.Admin.value:
            raise PermissionDenied(
                detail='Must be administrator to edit other users\' classifieds.'
            )

        try:
            price = int(price)
        except Exception:
            raise ValidationError(detail='Price not valid.')

        try:
            category_id = int(category_id)
        except Exception:
            raise ValidationError(detail='Category ID not valid.')

        try:
            for i, image_id in enumerate(image_ids):
                image_ids[i] = int(image_id)
        except Exception:
            raise ValidationError(detail='Image IDs not valid.')

        classified.title = title
        classified.description = description
        classified.price = price
        classified.category_id = category_id
        classified.save()

        classified.images.set(image_ids)

        content = ClassifiedModelSerializer(classified).data
        return Response(content)


class ClassifiedDestroyAPIView(generics.DestroyAPIView):
    authentication_classes = [TokenAuthentication, ]
    permission_classes = [permissions.IsAuthenticated, ]
    queryset = Classified.objects.all()

    def perform_destroy(self, instance):
        if self.request.user != instance.seller and self.request.user.role != UserRole.Admin.value:
            raise PermissionDenied(
                detail='Must be administrator to delete other users\' classifieds.'
            )

        instance.delete()


class ClassifiedUpdateDestroyAPIView(BaseManageView):
    VIEWS_BY_METHOD = {
        'PUT': ClassifiedUpdateAPIView.as_view,
        'DELETE': ClassifiedDestroyAPIView.as_view,
    }
