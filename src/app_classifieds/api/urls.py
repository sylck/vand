from django.urls import path

from .views import (
    ClassifiedIndexStoreAPIView,
    ClassifiedUpdateDestroyAPIView,
)


app_name = 'classifieds'

urlpatterns = [
    path('',          ClassifiedIndexStoreAPIView.as_view(),    name='index-store'),     # /api/classifieds/
    path('<int:pk>/', ClassifiedUpdateDestroyAPIView.as_view(), name='update-destroy'),  # /api/classifieds/42/
]
