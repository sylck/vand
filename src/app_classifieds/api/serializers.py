from rest_framework import serializers

from app_categories.api.serializers import CategoryModelSerializer
from app_images.api.serializers import ImageModelSerializer
from app_users.api.serializers import CustomUserModelSerializer
from ..models import Classified


class ClassifiedModelSerializer(serializers.ModelSerializer):
    category = CategoryModelSerializer()
    images = ImageModelSerializer(many=True)
    seller = CustomUserModelSerializer()

    class Meta:
        model = Classified
        fields = [
            'id',
            'title',
            'description',
            'price',
            'category',
            'images',
            'seller',
        ]
