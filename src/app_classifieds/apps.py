from django.apps import AppConfig


class AppClassifiedsConfig(AppConfig):
    name = 'app_classifieds'
