from django.contrib.auth import get_user_model
from django.db import models

from app_categories.models import Category
from app_images.models import Image

User = get_user_model()


class Classified(models.Model):
    title = models.CharField(max_length=70)
    description = models.TextField()
    price = models.IntegerField()
    category = models.ForeignKey(Category, on_delete=models.SET_NULL, related_name='classifieds', null=True)
    images = models.ManyToManyField(Image)
    seller = models.ForeignKey(User, on_delete=models.CASCADE, related_name='classifieds')
    date_posted = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return str(self.title)
