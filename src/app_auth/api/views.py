from rest_framework.authentication import BasicAuthentication
from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework.authtoken.models import Token
from rest_framework.exceptions import AuthenticationFailed, ValidationError
from rest_framework.parsers import FormParser

from vand.authentication import CSRFExemptSessionAuthentication

from app_users.models import CustomUser as User
from app_users.api.serializers import CustomUserModelSerializer


class LoginAPIView(APIView):
    authentication_classes = (CSRFExemptSessionAuthentication, BasicAuthentication)
    parser_classes = (FormParser,)

    def post(self, request, format=None):
        username = request.POST.get('username')
        password = request.POST.get('password')

        if not all([username, password]):
            raise AuthenticationFailed(detail='Username or password not provided.')

        user = User.objects.filter(username=username)

        if not user.exists():
            msg = f'User with username "{username}" does not exist.'
            raise AuthenticationFailed(detail=msg)

        user = user.first()

        if not user.check_password(password):
            raise AuthenticationFailed(detail='Invalid username or password')

        token, created = Token.objects.get_or_create(user=user)

        content = {
            'message': 'success',
            'user': CustomUserModelSerializer(user).data,
            'token': token.key
        }
        return Response(content)


class RegisterAPIView(APIView):
    def post(self, request, format=None):
        username = request.POST.get('username')
        email = request.POST.get('email')
        password = request.POST.get('password')
        county_id = request.POST.get('county')

        if not all([username, password, email, county_id]):
            raise ValidationError(detail='Incomplete data provided.')

        user = User.objects.filter(username=username)

        if user.exists():
            msg = f'User with username "{username}" already exists.'
            raise ValidationError(detail=msg)

        user_email = User.objects.filter(email=email)

        if user_email.exists():
            msg = f'User with e-mail "{email}" already exists.'
            raise ValidationError(detail=msg)

        new_user = User.objects.create(username=username, email=email, county_id=county_id)
        new_user.set_password(password)
        new_user.save()

        new_user_token, created = Token.objects.get_or_create(user=new_user)

        content = {
            'message': 'success',
            'user': CustomUserModelSerializer(new_user).data,
            'token': new_user_token.key
        }
        return Response(content)
