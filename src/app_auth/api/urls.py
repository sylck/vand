from django.urls import path

from .views import (
    LoginAPIView,
    RegisterAPIView,
)


app_name = 'api'

urlpatterns = [
    path('login/',    LoginAPIView.as_view(), name='login'),        # /api/auth/login/
    path('register/', RegisterAPIView.as_view(), name='register'),  # /api/auth/register/
]
