(function ($) {
    $.fn.characterCounter = function () {
        var cc = [];
        var containers = this.has('.character-count');

        for (var i = 0; i < containers.length; i++) {
            var container = containers.eq(i);
            cc.push(new CharacterCounter(container));
        }

        return this;
    }
}(jQuery));

function CharacterCounter(container) {
    /****************
     *    init()    *
     ****************/
    var textField = container.find("input[type='text'], textarea");
    var characterCountLabel = container.find('.character-count');

    var maxChars = parseInt(textField.attr('maxlength'));

    textField.on('change keydown cut paste', delayedFieldChangeHandler);
    /*
     * initialize characterCountLabel just in case its content is
     * invalid
     */
    fieldChangeHandler();


    /*******************
     *    functions    *
     *******************/
    function delayedFieldChangeHandler() {
        /*
         * field value retrieved in fieldChangeHandler lags one event
         * behind actual value. adding a 0 ms delay solves this
         * problem.
         *
         * source: https://stackoverflow.com/a/2055369
         */
        setTimeout(fieldChangeHandler, 0);
    }
    function fieldChangeHandler() {
        var chars = textField.val().length;
        characterCountLabel.text(chars + '/' + maxChars);
    }
}

/*
 * by default, Dropzone attempts to populate elements with .dropzone
 * class. automatic handling and population of <div> tags instead of
 * an expected <form> does not work well, so it has to be disabled.
 */
Dropzone.autoDiscover = false;
window.onload = main;

const FragmentType = {
    HOME:             0,
    ADMIN:            1,
    PROFILE:          2,
    ACCOUNT_SETTINGS: 3
};

// core building blocks
var auth;
var api;
var ui;

function main() {
    initAuth();
    initApi();
    initUi();
    initCharacterCounter();

    auth.tryAuthByCookie(ui.ready);

    //_DEBUG_FORCE_FAKE_ADMIN_AUTH();
    //_DEBUG_FORCE_FAKE_USER_AUTH();
    //_DEBUG_GO_TO_ADMIN();
    //_DEBUG_ADMIN_SWITCH_CATEGORIES_TAB();
    //_DEBUG_GO_TO_PROFILE();
    //_DEBUG_GO_TO_ACCOUNTSETTINGS();
}

function _DEBUG_FORCE_FAKE_ADMIN_AUTH() {
    auth.login(
        _DEBUG_GEN_FAKE_TOKEN(),
        {
            id: 0,
            username: 'adminuser',
            role: 1
        }
    );
}

function _DEBUG_FORCE_FAKE_USER_AUTH() {
    auth.login(
        _DEBUG_GEN_FAKE_TOKEN(),
        {
            id: 1,
            username: 'useruser',
            role: 0

        }
    );
}

function _DEBUG_GO_TO_ADMIN() {
    ui.goTo(FragmentType.ADMIN, {});
}

function _DEBUG_GO_TO_ACCOUNTSETTINGS() {
    ui.goTo(FragmentType.ACCOUNT_SETTINGS, {});
}

function _DEBUG_ADMIN_SWITCH_CATEGORIES_TAB() {
    $('#af-tab-categories').tab('show');
}

function _DEBUG_GO_TO_PROFILE() {
    var user = new User({
        id: 0,
        username: 'adminuser',
        role: auth.roles().ADMIN
    });
    ui.goTo(
        FragmentType.PROFILE,
        {user: user}
    );
}

function _DEBUG_GEN_FAKE_TOKEN() {
    return randomNChars(40);
}

function _DEBUG_DZ_ADD_RANDOM_IMAGE(selector) {
    var dropzone = Dropzone.forElement(selector);

    let image = {
        id: 6,
        url: 'http://lorempixel.com/1366/768/'
    };

    let file = {
        name: 'fakename.jpg',
        size: 10000,
        dataURL: 'http://lorempixel.com/1366/768/'
    };

    dropzone.files.push(file);
    dropzone.emit('addedfile', file);
    dropzone.createThumbnailFromUrl(
        file,
        dropzone.options.thumbnailWidth,
        dropzone.options.thumbnailHeight,
        dropzone.options.thumbnailMethod,
        true,
        function (thumbnail) {
            dropzone.emit('thumbnail', file, thumbnail);
            dropzone.emit('success', file, image);
            dropzone.emit('complete', file);
        }
    );
}

function _DEBUG_PRINT_EVENTS(el, msg) {
    var elDom = el[0];
    var elEvts = $._data(elDom, 'events');
    var allEvts = $._data(document, 'events');
    console.log({
        aMessage: msg,
        el: el,
        elDom: elDom,
        elEvts: elEvts,
        allEvts: allEvts
    });
}

function showModal(modal) {
    modal.modal('show');
}

function hideModal(modal) {
    modal.modal('hide');
}

function showElementJquery(el) {
    el.removeClass('hidden');
}

function hideElementJquery(el) {
    el.addClass('hidden');
}

function enableElementJquery(el) {
    el.removeClass('disabled');
}

function disableElementJquery(el) {
    el.addClass('disabled');
}

function enableButtonJquery(b) {
    b.prop('disabled', false);
}

function disableButtonJquery(b) {
    b.prop('disabled', true);
}

function _DEBUG_FORMDATA_TO_OBJ(formData) {
    // source: https://stackoverflow.com/a/8649003
    var formObj = JSON.parse(
        '{"' + formData.replace(/&/g, '","').replace(/=/g,'":"') + '"}',
        function(key, value) { return key===""?value:decodeURIComponent(value) }
    );

    return formObj;
}

function _DEBUG_PARSE_STR_TO_BOOL(str) {
    if (str === 'true') {
        return true;
    }
    else if (str === 'false') {
        return false;
    }
    else {
        return undefined;
    }
}

function randomNChars(n) {
    var chars = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789';
    var out = '';

    for (var i = 0; i < n; i++) {
        /*
         * ~~ is a faster alternative to Math.floor() and Math.trunc().
         * source: https://stackoverflow.com/a/38714503
         */
        var idx = ~~(Math.random() * (chars.length - 1));
        out = out + chars[idx];
    }

    return out;
}

function removeIntFromArray(arr, val) {
    return arr.filter(function (item) {
        return item !== val;
    });
}

function objectHasProperty(obj, key) {
    return isDefined(obj[key]);
}

function isDefined(arg) {
    return typeof(arg) !== "undefined";
}

function bindOneHandlerToEvent(el, event, handler) {
    el.off();
    el.on(event, handler);
}

function initUi() {
    ui = new Ui();
    ui.initFragments();
    ui.search().clearAll();
}

function Ui() {
    /****************
     *    init()    *
     ****************/
    // DOM elements
    var body = $('body');

    var navbar = new Navbar();
    var search = new Search();
    var fragmentManager = new FragmentManager();


    /*****************
     *    methods    *
     *****************/
    this.login = function () {
        navbar.login();
        fragmentManager.login();
    };

    this.logout = function () {
        navbar.logout();
        fragmentManager.logout();
    };

    this.search = function () {
        return search;
    };

    this.ready = function () {
        body.removeClass('loading');
    };

    this.initFragments = fragmentManager.initShowHome;
    this.goTo = fragmentManager.goTo;
    this.fragmentCanHandleItems = fragmentManager.canHandleItems;
    this.prepareFragmentForItems = fragmentManager.prepareForItems;
    this.setItems = fragmentManager.setItems;
}

function FragmentManager() {
    /****************
     *    init()    *
     ****************/
    // fragments
    var homeFragment = new HomeFragment();
    var adminFragment = new AdminFragment();
    var profileFragment = new ProfileFragment();
    var accountSettingsFragment = new AccountSettingsFragment();
    activeFragment = homeFragment;

    // register fragments here
    var fragments = [
        homeFragment,
        adminFragment,
        profileFragment,
        accountSettingsFragment
    ];

    var activeFragment;


    /*****************
     *    methods    *
     *****************/
    this.initShowHome = function () {
        hideAll();
        activeFragment.show();
    };

    this.logout = function () {
        if (activeFragment.loggedInOnly()) {
            switchTo(FragmentType.HOME, {});
        }
        else {
            activeFragment.logout();
        }
    };

    this.login = function () {
        activeFragment.login();
    };

    this.canHandleItems = function () {
        return activeFragment.canHandleItems();
    };

    this.prepareForItems = function () {
        activeFragment.prepareForItems();
    };

    this.setItems = function (items) {
        if (!activeFragment.canHandleItems()) {
            switchTo(FragmentType.HOME, {hasItems: true});
        }

        activeFragment.setItems(items);
    };

    this.goTo = switchTo;


    /*******************
     *    functions    *
     *******************/
    function hideAll() {
        for (var i = 0; i < fragments.length; i++) {
            fragments[i].hide();
        }
    }

    function switchTo(fragmentType, args) {
        var search = ui.search();

        activeFragment.hide();
        api.cancelPendingRequests();

        switch (fragmentType) {
            case FragmentType.HOME:
                /*
                 * re-run query with user filter removed if switching
                 * from ProfileFragment to HomeFragment to get
                 * classifieds from *all* users, not just the one
                 * whose profile has just been left.
                 */
                var comingFromProfile = activeFragment.fragmentType() === FragmentType.PROFILE;
                if (comingFromProfile) {
                    search.clearUserId();
                }

                var senderIsLogo = !!args.senderIsLogo;
                if (senderIsLogo) {
                    search.clearAll();
                }

                activeFragment = homeFragment;
                activeFragment.show();

                var hasItems = !!args.hasItems;

                var shouldRunSearch = comingFromProfile || senderIsLogo || !hasItems;
                if (shouldRunSearch) {
                    search.run();
                }

                break;
            case FragmentType.ADMIN:
                activeFragment = adminFragment;
                activeFragment.show();

                /*
                 * clear text query. it's not of much use in admin
                 * fragment and could be needlessly confusing to the
                 * end-user if search box retains value while
                 * switching.
                 */
                search.clearTextQuery();

                break;
            case FragmentType.ACCOUNT_SETTINGS:
                activeFragment = accountSettingsFragment;
                activeFragment.show();
                search.clearTextQuery();

                break;
            case FragmentType.PROFILE:
                var user = args.user;
                activeFragment = profileFragment;
                activeFragment.show(user);

                /*
                 * clearing all filters because going from
                 * HomeFragment to ProfileFragment would otherwise
                 * leave filters that have no place there. flushing all
                 * filters is also desirable when going from one
                 * user's profile to another, because the user does
                 * not expect the filters to be retained in that
                 * switch. that is, no condition exists in which
                 * search filters should not be cleared when going to
                 * ProfileFragment.
                 */
                search.clearAll();
                search.setUserId(user.id());
                search.run();

                break;
        }
    }
}

function HomeFragment() {
    /****************
     *    init()    *
     ****************/
    var fragmentType = FragmentType.HOME;
    var loggedInOnly = false;
    var canHandleItems = true;

    // DOM elements
    var container = $('#home-fragment');
    var createClassifiedFab = $('#fab-create-classified');

    // event binding
    createClassifiedFab.click(createClassifiedFabPressed);

    var classifiedGrid = new ClassifiedGrid(container);
    var filter = new Filter(container, true);


    /*****************
     *    methods    *
     *****************/
    this.hide = function () {
        hideElementJquery(container);
    };

    this.show = function () {
        filter.show();

        showElementJquery(container);
    };

    this.login = function () {
        classifiedGrid.login();
        ui.search().run();
        enableElementJquery(createClassifiedFab);
    };

    this.logout = function () {
        disableElementJquery(createClassifiedFab);
        classifiedGrid.logout();
        ui.search().run();
    };

    this.prepareForItems = function () {
        filter.prepareForItems();
        classifiedGrid.prepareForItems();
    };

    this.setItems = function (items) {
        filter.setItems();
        classifiedGrid.setItems(items);
    };

    this.fragmentType = function () {
        return fragmentType;
    };

    this.loggedInOnly = function () {
        return loggedInOnly;
    };

    this.canHandleItems = function () {
        return canHandleItems;
    };


    /*******************
     *    functions    *
     *******************/
    function createClassifiedFabPressed() {
        if (!auth.isLoggedIn()) {
            showLoginRegisterModal();
        }
        else {
            showCreateClassifiedModal();
        }
    }

    function showCreateClassifiedModal() {
        var modal = new ClassifiedCreateModal(rerunSearch);
        modal.show();
    }

    function rerunSearch() {
        ui.search().run();
    }

    function showLoginRegisterModal() {
        var modal = new LoginRegisterModal();
        modal.show();
    }
}

function ClassifiedGrid(container) {
    /****************
     *    init()    *
     ****************/
    var items;
    var itemsJq = [];

    // DOM elements
    var loadingMessage = container.find('.loading');
    var classifiedGrid = container.find('.grid');
    var noResultsMessage = container.find('.no-results');

    // event binding
    classifiedGrid.on('click', '.post', showClassifiedShowModal);
    classifiedGrid.on('click', '.post .edit', showClassifiedEditModal);
    classifiedGrid.on('click', '.post .delete', showClassifiedDeleteModal);

    // Mustache templates
    var cgItemTemplate = $('#cg-item').html();
    Mustache.parse(cgItemTemplate);

    setup();


    /*****************
     *    methods    *
     *****************/
    this.login = function () {
        for (var i = 0; i < itemsJq.length; i++) {
            var itemJq = itemsJq[i];

            var idHolder = itemJq.find('.post.card');
            var classifiedActions = idHolder.find('.actions');
            var id = idHolder.data('id');

            var item = getItemById(id);

            var userIsAdmin = auth.user().isAdmin();
            var userIsOwner = auth.user().id() === item.seller.id;

            var shouldShowClassifiedActions = userIsAdmin || userIsOwner;

            if (shouldShowClassifiedActions) {
                showElementJquery(classifiedActions);
            }
            else {
                hideElementJquery(classifiedActions);
            }
        }
    };

    this.logout = function () {
        var classifiedActions = $('.post.card .actions');
        hideElementJquery(classifiedActions);
    };

    this.prepareForItems = function () {
        showElementJquery(loadingMessage);
        hideElementJquery(classifiedGrid);
        hideElementJquery(noResultsMessage);
    };

    this.setItems = function (_items) {
        items = _items;

        // flush all existing items
        itemsJq = [];
        classifiedGrid.find('.grid-item').remove();

        hideElementJquery(loadingMessage);

        if (items.length !== 0) {
            showElementJquery(classifiedGrid);

            for (var i = 0; i < items.length; i++) {
                var item = items[i];

                var primaryImageUrl = undefined;

                if (item.images.length !== 0) {
                    primaryImageUrl = item.images[0].url;
                }

                var post = Mustache.render(cgItemTemplate, {
                    id: item.id,
                    title: item.title,
                    price: item.price,
                    primaryImageUrl: primaryImageUrl
                });

                var postJq = $(post);

                var userIsLoggedIn = auth.isLoggedIn();
                var userIsOwner = false;
                var userIsAdmin = false;

                if (userIsLoggedIn) {
                    userIsAdmin = auth.user().isAdmin();
                    userIsOwner = auth.user().id() === item.seller.id;
                }

                var postActions = postJq.find('.post .actions');

                var shouldShowClassifiedActions = userIsLoggedIn && (userIsAdmin || userIsOwner);
                if (shouldShowClassifiedActions) {
                    showElementJquery(postActions);
                }
                else {
                    hideElementJquery(postActions);
                }

                classifiedGrid.append(postJq);
                itemsJq.push(postJq);
                classifiedGrid.imagesLoaded().progress(reloadMasonry);
            }

            reloadMasonry();
        }
        else {
            showElementJquery(noResultsMessage);
        }
    };


    /*******************
     *    functions    *
     *******************/
    function setup() {
        initMasonry();
    }

    function initMasonry() {
        classifiedGrid.masonry({
            itemSelector: '.grid-item',
            columnWidth: '.grid-sizer'
        });
    }

    function reloadMasonry() {
        classifiedGrid.masonry('reloadItems');
        classifiedGrid.masonry('layout');
    }

    function showClassifiedShowModal(event) {
        var item = getItemById(getItemIdFromSenderItemEvent(event));

        var modal = new ClassifiedShowModal(item);
        modal.show();
    }

    function showClassifiedEditModal(event) {
        event.stopPropagation();

        var item = getItemById(getItemIdFromSenderButtonEvent(event));

        var modal = new ClassifiedEditModal(item, rerunSearch);
        modal.show();
    }

    function showClassifiedDeleteModal(event) {
        event.stopPropagation();

        var item = getItemById(getItemIdFromSenderButtonEvent(event));

        var modal = new ClassifiedDeleteModal(item, rerunSearch);
        modal.show();
    }

    function rerunSearch() {
        ui.search().run();
    }

    function getItemById(id) {
        for (var i = 0; i < items.length; i++) {
            var item = items[i];
            if (id === item.id) {
                return item;
            }
        }
    }

    function getItemIdFromSenderItemEvent(senderEvt) {
        var sender = $(senderEvt.currentTarget);
        var id = sender.data('id');

        return id;
    }

    function getItemIdFromSenderButtonEvent(senderEvt) {
        var sender = $(senderEvt.currentTarget);
        var idHolder = sender.parent().parent();
        var id = idHolder.data('id');

        return id;
    }
}

function ClassifiedCreateModal(_confirmCallback) {
    /****************
     *    init()    *
     ****************/
    var confirmCallback = _confirmCallback;

    var categories;

    var title;
    var price;
    var description;
    var category;
    var images;

    var prevVal = {};

    // DOM elements
    var modal = $('#modal-create-classified');
    modal.loadingMessage = modal.find('.loading');
    modal.form = modal.find('form');
    modal.form.title = $('#mcc-title');
    modal.form.titleDom = modal.form.title[0];
    modal.form.price = $('#mcc-price');
    modal.form.price.error = modal.form.price.parent().find('.invalid-feedback');
    modal.form.priceDom = modal.form.price[0];
    modal.form.description = $('#mcc-description');
    modal.form.descriptionDom = modal.form.description[0];
    modal.form.categoryDropdown = $('#mcc-category');
    modal.form.categoryDropdownDom = modal.form.categoryDropdown[0];
    modal.form.imagesContainer = $('#mcc-images');
    modal.form.imagesContainer.error = modal.form.imagesContainer.parent().find('.invalid-feedback');
    modal.form.createButton =  $('#mcc-create');
    modal.form.createButton.spinner = modal.form.createButton.find('.spinner-border');

    modal.form.images = new ImageField(modal.form.imagesContainer, fieldChanged);

    modal.form.price.error.required = 'This field is required.';
    modal.form.price.error.notInteger = 'Please enter an integer value.';
    modal.form.price.error.invalid = 'Entered value is invalid.';

    bindEvents();
    startModalSetup();


    /*****************
     *    methods    *
     *****************/
    this.show = function () {
        showModal(modal);
    };


    /*******************
     *    functions    *
     *******************/
    function startModalSetup() {
        hideElementJquery(modal.form);
        showElementJquery(modal.loadingMessage);
        clearValidation();

        freezeCreateButton();

        modal.form.title.val('');
        modal.form.title.change();

        modal.form.price.val('');

        modal.form.description.val('');
        modal.form.description.change();

        modal.form.categoryDropdown.empty();
        modal.form.categoryDropdown.append(
            document.createElement('option')
        );

        api.fetchCategories(categoriesFetched);
    }

    function clearValidation() {
        modal.form.removeClass('was-validated');
        modal.form.imagesContainer.removeClass('is-invalid');
    }

    function filteredFieldChangeHandler(event) {
        setTimeout(delayedFieldChangeHandler, 0, event);
    }

    function delayedFieldChangeHandler(event) {
        var senderJq = $(event.currentTarget);
        var key = senderJq.attr('id');
        var senderValue = senderJq.val();

        var prevValIsUndefined = prevVal[key] === undefined;
        var newValIsEmpty = senderValue.length === 0;
        var notAChange = prevValIsUndefined && newValIsEmpty;

        var valuesDiffer = prevVal[key] !== senderValue;

        var different = !notAChange && valuesDiffer;

        if (different) {
            fieldChanged();
        }

        prevVal[key] = senderValue;
    }

    function fieldChanged() {
        clearValidation();
        unfreezeCreateButton();
    }

    function categoriesFetched(success, _categories) {
        categories = _categories;

        finalizeModalSetup();
    }

    function finalizeModalSetup() {
        for (var i = 0; i < categories.length; i++) {
            var category = categories[i];

            var option = $(document.createElement('option'));
            option.prop('value', category.id);
            option.text(category.title);

            modal.form.categoryDropdown.append(option);
        }

        hideElementJquery(modal.loadingMessage);
        showElementJquery(modal.form);

        unfreezeCreateButton();
    }

    function submitHandler(event) {
        event.preventDefault();
        createConfirmed();
    }

    function freezeCreateButton() {
        disableButtonJquery(modal.form.createButton);
    }

    function unfreezeCreateButton() {
        enableButtonJquery(modal.form.createButton);
    }

    function showSpinner() {
        showElementJquery(modal.form.createButton.spinner);
    }

    function hideSpinner() {
        hideElementJquery(modal.form.createButton.spinner);
    }

    function createConfirmed() {
        freezeCreateButton();
        showSpinner();

        title = modal.form.title.val();
        /*
         * conversion to int - +value works faster and more reliably
         * than parseInt(value)
         *
         * source: http://phrogz.net/js/string_to_number.html
         */
        price = +modal.form.price.val();
        description = modal.form.description.val();
        category = modal.form.categoryDropdown.find('option:selected').val();
        images = modal.form.images.imageIds();

        startValidation();
    }

    function startValidation() {
        var errors = 0;

        var titleBuiltinValid = modal.form.titleDom.checkValidity();
        if (!titleBuiltinValid) {
            errors++;
        }

        var priceBuiltinValid = modal.form.priceDom.checkValidity();
        if (!priceBuiltinValid) {
            if (modal.form.priceDom.validity.valueMissing) {
                modal.form.price.error.text(modal.form.price.error.required);
            }
            else if (modal.form.priceDom.validity.stepMismatch) {
                modal.form.price.error.text(modal.form.price.error.notInteger);
            }
            else {
                modal.form.price.error.text(modal.form.price.error.invalid);
            }
            errors++;
        }

        var descriptionBuiltinValid = modal.form.descriptionDom.checkValidity();
        if (!descriptionBuiltinValid) {
            errors++;
        }

        var categoryDropdownBuiltinValid = modal.form.categoryDropdownDom.checkValidity();
        if (!categoryDropdownBuiltinValid) {
            errors++;
        }

        var imagesValid = modal.form.images.checkValidity();
        if (!imagesValid) {
            modal.form.imagesContainer.error.text(modal.form.images.getErrorStr());
            modal.form.imagesContainer.addClass('is-invalid');
            errors++;
        }

        if (errors !== 0) {
            validationFailed();
        }
        else {
            validationPassed();
        }
    }

    function validationFailed() {
        modal.form.addClass('was-validated');
        hideSpinner();
    }

    function validationPassed() {
        var params = {
            title: title,
            price: price,
            description: description,
            category: category,
            images: images
        };

        api.storeClassified(
            $.param(params),
            createProcessed
        );
    }

    function createProcessed() {
        hideModal(modal);
        confirmCallback();
    }

    function bindEvents() {
        modal.form.submit(submitHandler);

        modal.form.title.on('change keydown cut paste', filteredFieldChangeHandler);
        modal.form.price.on('change keydown cut paste', filteredFieldChangeHandler);
        modal.form.description.on('change keydown cut paste', filteredFieldChangeHandler);
        modal.form.categoryDropdown.change(fieldChanged);

        modal.on('hidden.bs.modal', hideHandler);
    }

    function hideHandler() {
        modal.form.images.hide();
        hideSpinner();
        unbindEvents();
    }

    function unbindEvents() {
        modal.form.off('submit', submitHandler);

        modal.form.title.off('change keydown cut paste', filteredFieldChangeHandler);
        modal.form.price.off('change keydown cut paste', filteredFieldChangeHandler);
        modal.form.description.off('change keydown cut paste', filteredFieldChangeHandler);
        modal.form.categoryDropdown.off('change', fieldChanged);

        modal.off('hidden.bs.modal', hideHandler);
    }
}

function ClassifiedShowModal(_classified) {
    /****************
     *    init()    *
     ****************/
    var classified = _classified;

    var seller = new User(classified.seller);

    // DOM elements
    var modal = $('#modal-view-classified');
    modal.carousel = $('#mvc-images-carousel');
    modal.carousel.indicators = modal.carousel.find('.carousel-indicators');
    modal.carousel.controlButtons = modal.carousel.find('.carousel-control-prev, .carousel-control-next');
    modal.carousel.images = modal.carousel.find('.carousel-inner');
    modal.title = modal.find('.title');
    modal.idText = modal.find('.id');
    modal.price = modal.find('.price');
    modal.descriptionText = modal.find('.description');
    modal.category = modal.find('.category');
    modal.seller = modal.find('.seller');
    modal.seller.username = modal.seller.find('.username');
    modal.seller.viewProfileButton = modal.seller.find('button');
    modal.seller.county = modal.seller.find('.county');
    modal.seller.email = modal.seller.find('.email');

    // event binding
    modal.seller.viewProfileButton.click(showSellerProfile);

    // Mustache templates
    var carouselIndicatorTemplate = '<li data-target="#mvc-images-carousel" data-slide-to="{{ i }}"></li>';
    var carouselImageTemplate = '<div class="carousel-item"><img class="d-block" src="{{ url }}"></div>';
    Mustache.parse(carouselIndicatorTemplate);
    Mustache.parse(carouselImageTemplate);

    setupModal();


    /*****************
     *    methods    *
     *****************/
    this.show = function () {
        showModal(modal);
    };


    /*******************
     *    functions    *
     *******************/
    function setupModal() {
        // setup carousel
        modal.carousel.indicators.empty();
        modal.carousel.images.empty();

        for (var i = 0; i < classified.images.length; i++) {
            var curImage = classified.images[i];

            var indicator = Mustache.render(carouselIndicatorTemplate, {
                i: i
            });
            var indicatorJq = $(indicator);

            var image = Mustache.render(carouselImageTemplate, {
                url: curImage.url
            });
            var imageJq = $(image);

            if (0 === i) {
                indicatorJq.addClass('active');
                imageJq.addClass('active');
            }

            modal.carousel.indicators.append(indicatorJq);
            modal.carousel.images.append(imageJq);
        }

        /*
         * using <= instead of === to handle image arrays of one element and
         * empty arrays for those cases when empty arrays manage to pass both
         * server-side and client-side validations
         */
        if (classified.images.length <= 1) {
            hideCarouselControls();
        }
        else {
            showCarouselControls();
        }

        modal.title.text(classified.title);
        modal.idText.text(classified.id);
        modal.price.text(classified.price);
        modal.descriptionText.html(
            htmlifyNewlines(classified.description)
        );
        modal.category.text(classified.category.title);

        modal.seller.username.text(classified.seller.username);
        modal.seller.county.text(classified.seller.county.name);
        modal.seller.email.text(classified.seller.email);
    }

    function hideCarouselControls() {
        hideElementJquery(modal.carousel.indicators);
        hideElementJquery(modal.carousel.controlButtons);
    }

    function showCarouselControls() {
        showElementJquery(modal.carousel.indicators);
        showElementJquery(modal.carousel.controlButtons);
    }

    function showSellerProfile() {
        hideModal(modal);
        ui.goTo(FragmentType.PROFILE, {user: seller});
    }

    function htmlifyNewlines(str) {
        var lineBreak = '<br>';
        return str.replace(/\r\n/g, lineBreak).replace(/\n/g, lineBreak);
    }
}

function ClassifiedEditModal(_classified, _confirmCallback) {
    /****************
     *    init()    *
     ****************/
    var classified = _classified;
    var confirmCallback = _confirmCallback;

    var categories;

    var title;
    var price;
    var description;
    var category;
    var images;

    var prevVal = {};

    // DOM elements
    var modal = $('#modal-edit-classified');
    modal.loadingMessage = modal.find('.loading');
    modal.form = modal.find('form');
    modal.form.title = $('#mec-title');
    modal.form.titleDom = modal.form.title[0];
    modal.form.price = $('#mec-price');
    modal.form.price.error = modal.form.price.parent().find('.invalid-feedback');
    modal.form.priceDom = modal.form.price[0];
    modal.form.description = $('#mec-description');
    modal.form.descriptionDom = modal.form.description[0];
    modal.form.categoryDropdown = $('#mec-category');
    modal.form.categoryDropdownDom = modal.form.categoryDropdown[0];
    modal.form.imagesContainer = $('#mec-images');
    modal.form.imagesContainer.error = modal.form.imagesContainer.parent().find('.invalid-feedback');
    modal.form.saveButton = $('#mec-save');
    modal.form.saveButton.spinner = modal.form.saveButton.find('.spinner-border');

    modal.form.images = new ImageField(modal.form.imagesContainer, fieldChanged);

    modal.form.price.error.required = 'This field is required.';
    modal.form.price.error.notInteger = 'Please enter an integer value.';
    modal.form.price.error.invalid = 'Entered value is invalid.';

    bindEvents();
    startModalSetup();


    /*****************
     *    methods    *
     *****************/
    this.show = function () {
        showModal(modal);
    };


    /*******************
     *    functions    *
     *******************/
    function startModalSetup() {
        hideElementJquery(modal.form);
        showElementJquery(modal.loadingMessage);
        clearValidation();

        freezeSaveButton();

        modal.form.title.val(classified.title);
        modal.form.title.change();

        modal.form.price.val(classified.price);

        modal.form.description.val(classified.description);
        modal.form.description.change();

        modal.form.categoryDropdown.empty();
        modal.form.categoryDropdown.append(
            document.createElement('option')
        );

        modal.form.images.setItems(classified.images);

        api.fetchCategories(categoriesFetched);
    }

    function clearValidation() {
        modal.form.removeClass('was-validated');
        modal.form.imagesContainer.removeClass('is-invalid');
    }

    function filteredFieldChangeHandler(event) {
        setTimeout(delayedFieldChangeHandler, 0, event);
    }

    function delayedFieldChangeHandler(event) {
        var senderJq = $(event.currentTarget);
        var key = senderJq.attr('id');
        var senderValue = senderJq.val();

        var prevValIsUndefined = prevVal[key] === undefined;
        var newValIsEmpty = senderValue.length === 0;
        var notAChange = prevValIsUndefined && newValIsEmpty;

        var valuesDiffer = prevVal[key] !== senderValue;

        var different = !notAChange && valuesDiffer;

        if (different) {
            fieldChanged();
        }

        prevVal[key] = senderValue;
    }

    function fieldChanged() {
        clearValidation();
        unfreezeSaveButton();
    }

    function categoriesFetched(success, _categories) {
        categories = _categories;

        finalizeModalSetup();
    }

    function finalizeModalSetup() {
        for (var i = 0; i < categories.length; i++) {
            var category = categories[i];

            var option = $(document.createElement('option'));
            option.prop('value', category.id);
            option.prop('selected', classified.category.id === category.id);

            option.text(category.title);

            modal.form.categoryDropdown.append(option);
        }

        hideElementJquery(modal.loadingMessage);
        showElementJquery(modal.form);

        unfreezeSaveButton();
    }

    function submitHandler(event) {
        event.preventDefault();
        saveConfirmed();
    }

    function freezeSaveButton() {
        disableButtonJquery(modal.form.saveButton);
    }

    function unfreezeSaveButton() {
        enableButtonJquery(modal.form.saveButton);
    }

    function showSpinner() {
        showElementJquery(modal.form.saveButton.spinner);
    }

    function hideSpinner() {
        hideElementJquery(modal.form.saveButton.spinner);
    }

    function saveConfirmed() {
        freezeSaveButton();
        showSpinner();

        title = modal.form.title.val();
        price = +modal.form.price.val();
        description = modal.form.description.val();
        category = modal.form.categoryDropdown.find('option:selected').val();
        images = modal.form.images.imageIds();

        startValidation();
    }

    function startValidation() {
        var errors = 0;

        var titleBuiltinValid = modal.form.titleDom.checkValidity();
        if (!titleBuiltinValid) {
            errors++;
        }

        var priceBuiltinValid = modal.form.priceDom.checkValidity();
        if (!priceBuiltinValid) {
            if (modal.form.priceDom.validity.valueMissing) {
                modal.form.price.error.text(modal.form.price.error.required);
            }
            else if (modal.form.priceDom.validity.stepMismatch) {
                modal.form.price.error.text(modal.form.price.error.notInteger);
            }
            else {
                modal.form.price.error.text(modal.form.price.error.invalid);
            }
            errors++;
        }

        var descriptionBuiltinValid = modal.form.descriptionDom.checkValidity();
        if (!descriptionBuiltinValid) {
            errors++;
        }

        var categoryDropdownBuiltinValid = modal.form.categoryDropdownDom.checkValidity();
        if (!categoryDropdownBuiltinValid) {
            errors++;
        }

        var imagesValid = modal.form.images.checkValidity();
        if (!imagesValid) {
            modal.form.imagesContainer.error.text(modal.form.images.getErrorStr());
            modal.form.imagesContainer.addClass('is-invalid');
            errors++;
        }

        if (errors !== 0) {
            validationFailed();
        }
        else {
            validationPassed();
        }
    }

    function validationFailed() {
        modal.form.addClass('was-validated');
        hideSpinner();
    }

    function validationPassed() {
        var params = {
            title: title,
            price: price,
            description: description,
            category: category,
            images: images
        };

        api.updateClassified(
            classified.id,
            $.param(params),
            saveProcessed
        );
    }

    function saveProcessed() {
        hideModal(modal);
        confirmCallback();
    }

    function bindEvents() {
        modal.form.submit(submitHandler);

        modal.form.title.on('change keydown cut paste', filteredFieldChangeHandler);
        modal.form.price.on('change keydown cut paste', filteredFieldChangeHandler);
        modal.form.description.on('change keydown cut paste', filteredFieldChangeHandler);
        modal.form.categoryDropdown.change(fieldChanged);

        modal.on('hidden.bs.modal', hideHandler);
    }

    function hideHandler() {
        modal.form.images.hide();
        hideSpinner();
        unbindEvents();
    }

    function unbindEvents() {
        modal.form.off('submit', submitHandler);

        modal.form.title.off('change keydown cut paste', filteredFieldChangeHandler);
        modal.form.price.off('change keydown cut paste', filteredFieldChangeHandler);
        modal.form.description.off('change keydown cut paste', filteredFieldChangeHandler);
        modal.form.categoryDropdown.off('change', fieldChanged);

        modal.off('hidden.bs.modal', hideHandler);
    }
}

function ImageField(_container, _changedCallback) {
    /****************
     *    init()    *
     ****************/
    const MAX_FILES_NUMBER = 10;
    const UUID_CHARS = 36;

    const VF_UPLOADSHAVEERRORS = 'uploadsHaveErrors',
          VF_UPLOADSPENDING    = 'uploadsPending',
          VF_NOIMAGESUPLOADED  = 'noImagesUploaded';

    const VALIDITY_FLAGS = [
        VF_UPLOADSHAVEERRORS,
        VF_UPLOADSPENDING,
        VF_NOIMAGESUPLOADED
    ];

    const MSG_VF_UPLOADSHAVEERRORS = 'One or more uploads have failed. Please ensure all uploads have been successful before proceeding.',
          MSG_VF_UPLOADSPENDING = 'There are uploads still in progress. Please wait for them to finish before proceeding.',
          MSG_VF_NOIMAGESUPLOADED = 'At least one image must be uploaded.';

    var container = _container;
    var changedCallback = _changedCallback;

    var validity;

    var apiImageStoreEndpoint = api.imageStoreEndpoint();
    var removeButtonHtml = '<span class="btn btn-sm btn-elegant px-3"><i class="fa fa-times-circle"></i></span>';

    var dropzone;
    var images = [];

    setup();


    /*****************
     *    methods    *
     *****************/
    this.setItems = function (items) {
        for (var i = 0; i < items.length; i++) {
            var image = items[i];

            var uuid = randomNChars(UUID_CHARS);
            image.uuid = uuid;

            /*
             * official docs are not updated for dropzone v5, solution
             * found here: https://github.com/enyo/dropzone/issues/1587
             */
            var file = {
                name: getFilenameFromUrl(image.url),
                size: image.size,

                /*
                 * necessary because maxFiles is compared against **accepted**
                 * files
                 */
                accepted: true,

                dataURL: image.url,
                upload: {
                    uuid: uuid
                }
            };

            dropzone.files.push(file);
            dropzone.emit('addedfile', file);
            createThumbnailForFile(file);

            images.push(image);
        }
    };

    this.imageIds = function () {
        var imageIds = [];

        for (var i = 0; i < images.length; i++) {
            imageIds.push(images[i].id);
        }

        return imageIds;
    };

    this.checkValidity = function () {
        resetValidityFlags();
        var errors = 0;

        if (checkFilesForStatus('error')) {
            validity[VF_UPLOADSHAVEERRORS] = true;
            errors++;
        }

        if (checkFilesForStatus('uploading')) {
            validity[VF_UPLOADSPENDING] = true;
            errors++;
        }

        if (images.length === 0) {
            validity[VF_NOIMAGESUPLOADED] = true;
            errors++;
        }

        return errors === 0;
    };

    this.getErrorStr = function () {
        if (validity[VF_UPLOADSHAVEERRORS]) {
            return MSG_VF_UPLOADSHAVEERRORS;
        }
        else if (validity[VF_UPLOADSPENDING]) {
            return MSG_VF_UPLOADSPENDING;
        }
        else {
            return MSG_VF_NOIMAGESUPLOADED;
        }
    };

    this.hide = function () {
        dropzone.destroy();
    };


    /*******************
     *    functions    *
     *******************/
    function setup() {
        attachDropzone();

        dropzone = getDropzone();
        dropzone.removeAllFiles(true);

        dropzone.on('sending', appendToken);
        dropzone.on('addedfile', imageAdded);
        dropzone.on('success', imageSuccess);
        dropzone.on('removedfile', imageRemoved);
    }

    function attachDropzone() {
        if (!dropzoneExists()) {
            container.dropzone({
                method: "put",
                url: apiImageStoreEndpoint,
                paramName: "image",
                acceptedFiles: "image/*",
                maxFiles: MAX_FILES_NUMBER,
                addRemoveLinks: true,
                dictCancelUpload: removeButtonHtml,
                dictRemoveFile: removeButtonHtml
            });
        }
    }

    function dropzoneExists() {
        return !!getDropzone();
    }

    function getDropzone() {
        return container[0].dropzone;
    }

    function createThumbnailForFile(file) {
        /*
         * separate function to work around 'mutable variable is
         * accessible from closure' warnings and having to use 'let'
         *
         * credits: https://stackoverflow.com/a/25639187
         */
        dropzone.createThumbnailFromUrl(
            file,
            dropzone.options.thumbnailWidth,
            dropzone.options.thumbnailHeight,
            dropzone.options.thumbnailMethod,
            true,
            function (thumbnail) {
                dropzone.emit('thumbnail', file, thumbnail);
                dropzone.emit('complete', file);
            }
        );
    }

    function getFilenameFromUrl(url) {
        // credits: https://befused.com/javascript/get-filename-url
        return url.substring(url.lastIndexOf('/') + 1);
    }

    function resetValidityFlags() {
        validity = {};

        for (var i = 0; i < VALIDITY_FLAGS.length; i++) {
            var flag = VALIDITY_FLAGS[i];
            validity[flag] = false;
        }
    }

    function appendToken(file, xhr, formData) {
        xhr.setRequestHeader(
            'Authorization',
            'Token %token%'.replace(/%token%/g, auth.token())
        );
    }

    function imageAdded() {
        changedCallback();
    }

    function imageSuccess(image, serverResponse) {
        images.push({
            uuid: image.upload.uuid,
            id: serverResponse.id,
            url: serverResponse.url
        });

        changedCallback();
    }

    function imageRemoved(image) {
        removeImageByUuid(image.upload.uuid);

        changedCallback();
    }

    function removeImageByUuid(uuid) {
        images = images.filter(
            function (image) { return image.uuid !== uuid; }
        );
    }

    function checkFilesForStatus(status) {
        for (var i = 0; i < dropzone.files.length; i++) {
            var f = dropzone.files[i];
            if (status === f.status) {
                return true;
            }
        }

        return false;
    }
}

function ClassifiedDeleteModal(_classified, _confirmCallback) {
    /****************
     *    init()    *
     ****************/
    var classified = _classified;
    var confirmCallback = _confirmCallback;

    // DOM elements
    var modal = $('#modal-delete-classified');
    modal.titleLabel = modal.find('.title');
    modal.idLabel = modal.find('.id');
    modal.deleteButton = $('#mdc-delete');
    modal.deleteButton.spinner = modal.deleteButton.find('.spinner-border');

    setupModal();


    /*****************
     *    methods    *
     *****************/
    this.show = function () {
        showModal(modal);
    };


    /*******************
     *    functions    *
     *******************/
    function setupModal() {
        unfreezeDeleteButton();
        modal.titleLabel.text(classified.title);
        modal.idLabel.text(classified.id);
    }

    function unfreezeDeleteButton() {
        bindOneHandlerToEvent(modal.deleteButton, 'click', deleteConfirmed);
        enableElementJquery(modal.deleteButton);
    }

    function freezeDeleteButton() {
        modal.deleteButton.off();
        disableElementJquery(modal.deleteButton);
    }

    function showSpinner() {
        showElementJquery(modal.deleteButton.spinner);
    }

    function hideSpinner() {
        hideElementJquery(modal.deleteButton.spinner);
    }

    function deleteConfirmed() {
        freezeDeleteButton();
        showSpinner();

        api.destroyClassified(classified.id, deleteProcessed);
    }

    function deleteProcessed() {
        hideSpinner();
        hideModal(modal);
        confirmCallback();
    }
}

function LoginRegisterModal() {
    /****************
     *    init()    *
     ****************/
    var loginTab = new LrmLoginTab(authSuccessful);
    var registerTab = new LrmRegisterTab(authSuccessful);
    var activeTab;

    // DOM elements
    var modal = $('#modal-login-register');
    modal.loginTabLink = $('#mlr-tab-login');
    modal.registerTabLink = $('#mlr-tab-register');

    bindEvents();


    /*****************
     *    methods    *
     *****************/
    this.show = function () {
        // force showing login tab to synchronize UI and code
        modal.loginTabLink.tab('show');
        /*
         * manually trigger tab click event just in case switching did
         * not happen, as is the case when login modal is opened for
         * the first time.
         */
        modal.loginTabLink.trigger('show.bs.tab');
        showModal(modal);
    };


    /*******************
     *    functions    *
     *******************/
    function activeTabChanged() {
        api.cancelPendingRequests();
        activeTab.show();
    }

    function showLoginTab() {
        activeTab = loginTab;
        activeTabChanged();
    }

    function showRegisterTab() {
        activeTab = registerTab;
        activeTabChanged();
    }

    function authSuccessful(token, userPo) {
        hideModal(modal);
        auth.login(token, userPo);
    }

    function bindEvents() {
        modal.loginTabLink.on('show.bs.tab', showLoginTab);
        modal.registerTabLink.on('show.bs.tab', showRegisterTab);

        modal.on('hidden.bs.modal', hideHandler);
    }

    function hideHandler() {
        loginTab.hide();
        registerTab.hide();
        unbindEvents();
    }

    function unbindEvents() {
        modal.loginTabLink.off('show.bs.tab', showLoginTab);
        modal.registerTabLink.off('show.bs.tab', showRegisterTab);

        modal.off('hidden.bs.modal', hideHandler);
    }
}

function LrmLoginTab(successCallback) {
    /****************
     *    init()    *
     ****************/
    var authSuccessful = successCallback;

    var username;
    var password;

    var prevVal = {};

    // DOM elements
    var tab = $('#mlr-login');
    tab.form = tab.find('form');
    tab.form.invalidAlert = tab.form.find('.alert');
    tab.form.invalidAlert.closeButton = tab.form.invalidAlert.find('button');
    tab.form.username = $('#mlr-login-username');
    tab.form.usernameDom = tab.form.username[0];
    tab.form.password = $('#mlr-login-password');
    tab.form.passwordDom = tab.form.password[0];
    tab.form.loginButton = $('#mlr-login-submit');
    tab.form.loginButton.spinner = tab.form.loginButton.find('.spinner-border');

    bindEvents();


    /*****************
     *    methods    *
     *****************/
    this.show = function () {
        setupTab();
    };

    this.hide = function () {
        hideHandler();
    };


    /*******************
     *    functions    *
     *******************/
    function setupTab() {
        hideInvalidUsernamePasswordAlert();
        clearValidation();

        freezeLoginButton();

        tab.form.username.val('');
        tab.form.password.val('');

        unfreezeLoginButton();
    }

    function clearValidation() {
        tab.form.removeClass('was-validated');
    }

    function filteredFieldChangeHandler(event) {
        setTimeout(delayedFieldChangeHandler, 0, event);
    }

    function delayedFieldChangeHandler(event) {
        var senderJq = $(event.currentTarget);
        var key = senderJq.attr('id');
        var senderValue = senderJq.val();

        var prevValIsUndefined = prevVal[key] === undefined;
        var newValIsEmpty = senderValue.length === 0;
        var notAChange = prevValIsUndefined && newValIsEmpty;

        var valuesDiffer = prevVal[key] !== senderValue;

        var different = !notAChange && valuesDiffer;

        if (different) {
            fieldChanged();
        }

        prevVal[key] = senderValue;
    }

    function fieldChanged() {
        clearValidation();
        unfreezeLoginButton();
    }

    function submitHandler(event) {
        event.preventDefault();
        loginConfirmed();
    }

    function freezeLoginButton() {
        disableButtonJquery(tab.form.loginButton);
    }

    function unfreezeLoginButton() {
        enableButtonJquery(tab.form.loginButton);
    }

    function showSpinner() {
        showElementJquery(tab.form.loginButton.spinner);
    }

    function hideSpinner() {
        hideElementJquery(tab.form.loginButton.spinner);
    }

    function loginConfirmed() {
        freezeLoginButton();
        showSpinner();
        hideInvalidUsernamePasswordAlert();

        username = tab.form.username.val();
        password = tab.form.password.val();

        startValidation();
    }

    function startValidation() {
        var errors = 0;

        var usernameBuiltinValid = tab.form.usernameDom.checkValidity();
        if (!usernameBuiltinValid) {
            errors++;
        }

        var passwordBuiltinValid = tab.form.passwordDom.checkValidity();
        if (!passwordBuiltinValid) {
            errors++;
        }

        if (errors !== 0) {
            validationFailed();
        }
        else {
            validationPassed();
        }
    }

    function validationFailed() {
        tab.form.addClass('was-validated');
        hideSpinner();
    }

    function validationPassed() {
        var params = {
            username: username,
            password: password
        };

        api.login(
            $.param(params),
            loginProcessed
        );
    }

    function loginProcessed(success, response) {
        if (!success) {
            showInvalidUsernamePasswordAlert();
            hideSpinner();
            return;
        }

        // else authentication successful
        var token = response.token;
        var userPo = response.user;

        authSuccessful(token, userPo);
    }

    function showInvalidUsernamePasswordAlert() {
        showElementJquery(tab.form.invalidAlert);
    }

    function hideInvalidUsernamePasswordAlert() {
        hideElementJquery(tab.form.invalidAlert);
    }

    function bindEvents() {
        tab.form.invalidAlert.closeButton.click(hideInvalidUsernamePasswordAlert);

        tab.form.submit(submitHandler);

        tab.form.username.on('change keydown cut paste', filteredFieldChangeHandler);
        tab.form.password.on('change keydown cut paste', filteredFieldChangeHandler);
    }

    function hideHandler() {
        hideSpinner();
        unbindEvents();
    }

    function unbindEvents() {
        tab.form.invalidAlert.closeButton.off('click', hideInvalidUsernamePasswordAlert);

        tab.form.off('submit', submitHandler);

        tab.form.username.off('change keydown cut paste', filteredFieldChangeHandler);
        tab.form.password.off('change keydown cut paste', filteredFieldChangeHandler);
    }
}

function LrmRegisterTab(successCallback) {
    /****************
     *    init()    *
     ****************/
    const JOB_VALIDATE_USERNAME = 1,
          JOB_VALIDATE_EMAIL    = 2;

    var authSuccessful = successCallback;

    var remainingJobs = [];
    var counties;
    var errors;

    var username;
    var email;
    var password;
    var county;

    var prevVal = {};

    // DOM elements
    var tab = $('#mlr-register');
    tab.loadingMessage = tab.find('.loading');
    tab.form = tab.find('form');
    tab.form.username = $('#mlr-register-username');
    tab.form.username.error = tab.form.username.parent().find('.invalid-feedback');
    tab.form.usernameDom = tab.form.username[0];
    tab.form.email = $('#mlr-register-email');
    tab.form.email.error = tab.form.email.parent().find('.invalid-feedback');
    tab.form.emailDom = tab.form.email[0];
    tab.form.password = $('#mlr-register-password');
    tab.form.countyDropdown = $('#mlr-register-county');
    tab.form.countyDropdownDom = tab.form.countyDropdown[0];
    tab.form.registerButton = $('#mlr-register-submit');
    tab.form.registerButton.spinner = tab.form.registerButton.find('.spinner-border');

    tab.form.password.cls = new TogglePasswordField(tab.form.password);

    tab.form.username.regex = /^[a-zA-Z0-9_\-\.]+$/;
    tab.form.username.error.required = 'This field is required.';
    tab.form.username.error.regexMismatch = 'Username must be a string with a minimum of 1 character and a maximum of %maxlength% characters. Allowed characters include alphanumeric characters (lowercase and uppercase), dash, underscore and period.';
    tab.form.username.error.usernameAlreadyTaken = 'Another user with name "%username%" already exists.';
    tab.form.email.error.required = tab.form.username.error.required;
    tab.form.email.error.invalid = 'Entered value is invalid.';
    tab.form.email.error.emailAlreadyTaken = 'Another user with e-mail "%email%" already exists.';

    bindEvents();


    /*****************
     *    methods    *
     *****************/
    this.show = function () {
        startTabSetup();
    };

    this.hide = function () {
        hideHandler();
    };


    /*******************
     *    functions    *
     *******************/
    function startTabSetup() {
        hideElementJquery(tab.form);
        showElementJquery(tab.loadingMessage);
        clearValidation();

        freezeRegisterButton();

        tab.form.username.val('');

        tab.form.email.val('');

        tab.form.password.cls.clear();

        tab.form.countyDropdown.empty();
        tab.form.countyDropdown.append(
            document.createElement('option')
        );

        api.fetchCounties(countiesFetched);
    }

    function clearValidation() {
        tab.form.removeClass('was-validated');
        tab.form.usernameDom.setCustomValidity('');
        tab.form.emailDom.setCustomValidity('');
    }

    function filteredFieldChangeHandler(event) {
        setTimeout(delayedFieldChangeHandler, 0, event);
    }

    function delayedFieldChangeHandler(event) {
        var senderJq = $(event.currentTarget);
        var key = senderJq.attr('id');
        var senderValue = senderJq.val();

        var prevValIsUndefined = prevVal[key] === undefined;
        var newValIsEmpty = senderValue.length === 0;
        var notAChange = prevValIsUndefined && newValIsEmpty;

        var valuesDiffer = prevVal[key] !== senderValue;

        var different = !notAChange && valuesDiffer;

        if (different) {
            fieldChanged();
        }

        prevVal[key] = senderValue;
    }

    function fieldChanged() {
        clearValidation();
        unfreezeRegisterButton();
    }

    function countiesFetched(success, _counties) {
        counties = _counties;

        finalizeTabSetup();
    }

    function finalizeTabSetup() {
        for (var i = 0; i < counties.length; i++) {
            var county = counties[i];

            var option = $(document.createElement('option'));
            option.prop('value', county.id);
            option.text(county.name);

            tab.form.countyDropdown.append(option);
        }

        hideElementJquery(tab.loadingMessage);
        showElementJquery(tab.form);

        unfreezeRegisterButton();
    }

    function submitHandler(event) {
        event.preventDefault();
        registerConfirmed();
    }

    function freezeRegisterButton() {
        disableButtonJquery(tab.form.registerButton);
    }

    function unfreezeRegisterButton() {
        enableButtonJquery(tab.form.registerButton);
    }

    function showSpinner() {
        showElementJquery(tab.form.registerButton.spinner);
    }

    function hideSpinner() {
        hideElementJquery(tab.form.registerButton.spinner);
    }

    function registerConfirmed() {
        freezeRegisterButton();
        showSpinner();

        username = tab.form.username.val();
        email = tab.form.email.val();
        password = tab.form.password.val();
        county = parseInt(tab.form.countyDropdown.find('option:selected').val());

        startValidation();
    }

    function startValidation() {
        errors = 0;

        var usernameBuiltinValid = tab.form.usernameDom.checkValidity();
        var usernameRegexValid = tab.form.username.regex.test(username);
        if (!usernameBuiltinValid) {
            tab.form.username.error.text(tab.form.username.error.required);
            errors++;
        }
        else if (!usernameRegexValid) {
            var msg = tab.form.username.error.regexMismatch;
            var maxlength = tab.form.username.attr('maxlength');
            msg = msg.replace(/%maxlength%/g, maxlength);
            tab.form.username.error.text(msg);
            tab.form.usernameDom.setCustomValidity(msg);

            errors++;
        }
        else {
            remainingJobs.push(JOB_VALIDATE_USERNAME);

            var params = {
                q: username,
                exact: true
            };
            api.fetchUsers(
                usernameExists,
                $.param(params)
            );
        }

        var emailBuiltinValid = tab.form.emailDom.checkValidity();
        if (!emailBuiltinValid) {
            if (tab.form.emailDom.validity.valueMissing) {
                tab.form.email.error.text(tab.form.email.error.required);
            }
            else {
                tab.form.email.error.text(tab.form.email.error.invalid);
            }
            errors++;
        }
        else {
            remainingJobs.push(JOB_VALIDATE_EMAIL);

            var params = {
                email: email,
                exact: true
            };
            api.fetchUsers(
                emailExists,
                $.param(params)
            );
        }

        var passwordBuiltinValid = tab.form.password.cls.checkValidity();
        if (!passwordBuiltinValid) {
            errors++;
        }

        var countyDropdownBuiltinValid = tab.form.countyDropdownDom.checkValidity();
        if (!countyDropdownBuiltinValid) {
            errors++;
        }

        if (remainingJobs.length === 0) {
            validationFailed();
        }
    }

    function usernameExists(success, users) {
        var exists = users.length !== 0;

        remainingJobs = removeIntFromArray(remainingJobs, JOB_VALIDATE_USERNAME);

        if (exists) {
            var msg = tab.form.username.error.usernameAlreadyTaken;
            msg = msg.replace(/%username%/g, username);
            tab.form.username.error.text(msg);
            tab.form.usernameDom.setCustomValidity(msg);

            errors++
        }

        if (remainingJobs.length === 0) {
            if (errors !== 0) {
                validationFailed();
            }
            else {
                validationPassed();
            }
        }
    }

    function emailExists(success, usersWithEmail) {
        var exists = usersWithEmail.length !== 0;

        remainingJobs = removeIntFromArray(remainingJobs, JOB_VALIDATE_EMAIL);

        if (exists) {
            var msg = tab.form.email.error.emailAlreadyTaken;
            msg = msg.replace(/%email%/g, email);
            tab.form.email.error.text(msg);
            tab.form.emailDom.setCustomValidity(msg);

            errors++;
        }

        if (remainingJobs.length === 0) {
            if (errors !== 0) {
                validationFailed();
            }
            else {
                validationPassed();
            }
        }
    }

    function validationFailed() {
        tab.form.addClass('was-validated');
        hideSpinner();
    }

    function validationPassed() {
        var params = {
            username: username,
            email: email,
            password: password,
            county: county
        };

        api.register(
            $.param(params),
            registerProcessed
        );
    }

    function registerProcessed(success, response) {
        /*
         * assuming all possible errors are previously eliminated by
         * validating
         */
        var token = response.token;
        var userPo = response.user;

        authSuccessful(token, userPo);
    }

    function bindEvents() {
        tab.form.submit(submitHandler);

        tab.form.username.on('change keydown cut paste', filteredFieldChangeHandler);
        tab.form.email.on('change keydown cut paste', filteredFieldChangeHandler);
        tab.form.password.on('change keydown cut paste', filteredFieldChangeHandler);
        tab.form.countyDropdown.change(fieldChanged);

        tab.form.password.cls.bindEvents();
    }

    function hideHandler() {
        hideSpinner();
        unbindEvents();
    }

    function unbindEvents() {
        tab.form.off('submit', submitHandler);

        tab.form.username.off('change keydown cut paste', filteredFieldChangeHandler);
        tab.form.email.off('change keydown cut paste', filteredFieldChangeHandler);
        tab.form.password.off('change keydown cut paste', filteredFieldChangeHandler);
        tab.form.countyDropdown.off('change', fieldChanged);

        tab.form.password.cls.unbindEvents();
    }
}

function TogglePasswordField(_field) {
    /****************
     *    init()    *
     ****************/
    const STATE_PASSWORD = 1,
          STATE_TEXT     = 2;

    var state;

    // DOM elements
    var field = _field;
    var fieldDom = field[0];
    var fieldContainer = field.parent();
    var toggleButton = field.parent().find('.toggle');
    toggleButton.icon = toggleButton.find('i');

    setup();


    /*****************
     *    methods    *
     *****************/
    this.clear = function () {
        field.val('');
        setState(STATE_PASSWORD);
    };

    this.bindEvents = function () {
        toggleButton.click(toggle);
    };

    this.unbindEvents = function () {
        toggleButton.off('click', toggle);
    };

    this.checkValidity = function () {
        var fieldValid = fieldDom.checkValidity();

        if (fieldValid) {
            fieldContainer.removeClass('is-invalid');
        }
        else {
            fieldContainer.addClass('is-invalid');
        }

        return fieldValid;
    };


    /*******************
     *    functions    *
     *******************/
    function setup() {
        setState(STATE_PASSWORD);
    }

    function setState(_state) {
        if (STATE_TEXT === _state) {
            field.prop('type', 'text');

            toggleButton.icon.removeClass('fa-eye');
            toggleButton.icon.addClass('fa-eye-slash');

            state = STATE_TEXT;
        }
        else {
            field.prop('type', 'password');

            toggleButton.icon.removeClass('fa-eye-slash');
            toggleButton.icon.addClass('fa-eye');

            state = STATE_PASSWORD;
        }
    }

    function toggle() {
        if (STATE_TEXT === state) {
            setState(STATE_PASSWORD);
        }
        else {
            setState(STATE_TEXT);
        }
    }
}

function AdminFragment() {
    /****************
     *    init()    *
     ****************/
    var fragmentType = FragmentType.ADMIN;
    var loggedInOnly = true;
    var canHandleItems = false;

    var usersTab = new AFUsersTab();
    var categoriesTab = new AFCategoriesTab();
    var countiesTab = new AFCountiesTab();
    var activeTab = usersTab;

    // DOM elements
    var container = $('#admin-fragment');
    var usersTabLink = $('#af-tab-users');
    var categoriesTabLink = $('#af-tab-categories');
    var countiesTabLink = $('#af-tab-counties');

    // event binding
    usersTabLink.on('show.bs.tab', showUsersTab);
    categoriesTabLink.on('show.bs.tab', showCategoriesTab);
    countiesTabLink.on('show.bs.tab', showCountiesTab);


    /*****************
     *    methods    *
     *****************/
    this.hide = function () {
        hideElementJquery(container);
    };

    this.show = function () {
        activeTab.show();
        showElementJquery(container);
    };

    this.fragmentType = function () {
        return fragmentType;
    };

    this.loggedInOnly = function () {
        return loggedInOnly;
    };

    this.canHandleItems = function () {
        return canHandleItems;
    };

    /*
     * this fragment is not viewable if not logged in, so these
     * methods can safely do nothing
     */
    this.login = function () {};
    this.logout = function () {};

    /*
     * same as above, except in this case the cause for safely doing
     * nothing is that this fragment can not handle items retrieved by
     * search
     */
    this.prepareForItems = function () {};


    /*******************
     *    functions    *
     *******************/
    function activeTabChanged() {
        api.cancelPendingRequests();
        activeTab.show();
    }

    function showUsersTab() {
        activeTab = usersTab;
        activeTabChanged();
    }

    function showCategoriesTab() {
        activeTab = categoriesTab;
        activeTabChanged();
    }

    function showCountiesTab() {
        activeTab = countiesTab;
        activeTabChanged();
    }
}

function AFUsersTab() {
    /****************
     *    init()    *
     ****************/
    var users = [];

    // DOM elements
    var tab = $('#af-users');
    tab.loadingMessage = tab.find('.loading');
    tab.table = tab.find('table');
    tab.table.tbody = tab.table.find('tbody');

    // event binding
    tab.table.tbody.on('click', '.edit', showUserEditModal);
    tab.table.tbody.on('click', '.delete', showUserDeleteModal);

    // Mustache templates
    var rowTemplate = $('#af-users-table-row').html();
    Mustache.parse(rowTemplate);

    var emptyRowTemplate = '<tr><td class="text-center" colspan="3">No users.</td></tr>';


    /*****************
     *    methods    *
     *****************/
    this.show = function () {
        startTableReload();
    };


    /*******************
     *    functions    *
     *******************/
    function populateTable() {
        tab.table.tbody.empty();

        for (var i = 0; i < users.length; i++) {
            var user = users[i];

            var row = Mustache.render(rowTemplate, {
                id: user.id,
                username: user.username,
                role: auth.roleIntToStr(user.role)
            });

            tab.table.tbody.append(row);
        }

        if (users.length === 0) {
            tab.table.tbody.append(emptyRowTemplate);
        }

        hideElementJquery(tab.loadingMessage);
        showElementJquery(tab.table);
    }

    function startTableReload() {
        hideElementJquery(tab.table);
        showElementJquery(tab.loadingMessage);

        var params = {
            except: auth.user().id()
        };
        api.fetchUsers(
            usersFetched,
            $.param(params)
        );
    }

    function usersFetched(success, _users) {
        users = _users;

        populateTable();
    }

    function showUserEditModal(event) {
        var user = getUserById(getUserIdFromSenderEvent(event));

        var modal = new UserEditModalAdmin(user, startTableReload);
        modal.show();
    }

    function showUserDeleteModal(event) {
        var user = getUserById(getUserIdFromSenderEvent(event));

        var modal = new UserDeleteModal(user.id, user.username, startTableReload);
        modal.show();
    }

    function getUserIdFromSenderEvent(senderEvt) {
        var sender = $(senderEvt.currentTarget);
        /*
         * we need to get the data-id value, which is stored in the
         * <tr> element. hierarchy is as follows:
         *
         * <tr data-id="...">
         *   <td>
         *     <div class="btn-group ...">
         *       <button></button> <!-- this is the event sender -->
         *     </div>
         *   </td>
         * </tr>
         *
         * to get to the <tr> element, we need to get the parent
         * (<tr>) of the parent (<td>) of the parent (<div>) of the
         * <button>.
         */
        var idHolder = sender.parent().parent().parent();
        var id = idHolder.data('id');

        return id;
    }

    function getUserById(id) {
        for (var i = 0; i < users.length; i++) {
            var user = users[i];
            if (id === user.id) {
                return user;
            }
        }
    }
}

function UserDeleteModal(_userId, _userName, _confirmCallback) {
    /****************
     *    init()    *
     ****************/
    var userId = _userId;
    var userName = _userName;
    var confirmCallback = _confirmCallback;

    // DOM elements
    var modal = $('#modal-delete-user');
    modal.usernameLabel = modal.find('.username');
    modal.deleteButton = $('#mdu-delete');
    modal.deleteButton.spinner = modal.deleteButton.find('.spinner-border');

    setupModal();


    /*****************
     *    methods    *
     *****************/
    this.show = function () {
        showModal(modal);
    };


    /*******************
     *    functions    *
     *******************/
    function setupModal() {
        unfreezeDeleteButton();
        modal.usernameLabel.text(userName);
    }

    function deleteConfirmed() {
        /*
         * unbind events from deleteButton - button is already pressed,
         * subsequent presses are unnecessary
         */
        freezeDeleteButton();
        showSpinner();

        api.destroyUser(userId, deleteProcessed);
    }

    function unfreezeDeleteButton() {
        modal.deleteButton.click(deleteConfirmed);
        enableElementJquery(modal.deleteButton);
    }

    function freezeDeleteButton() {
        modal.deleteButton.off();
        disableElementJquery(modal.deleteButton);
    }

    function showSpinner() {
        showElementJquery(modal.deleteButton.spinner);
    }

    function hideSpinner() {
        hideElementJquery(modal.deleteButton.spinner);
    }

    function deleteProcessed() {
        hideSpinner();
        hideModal(modal);
        confirmCallback();
    }
}

function UserEditModalAdmin(_userPo, _confirmCallback) {
    /****************
     *    init()    *
     ****************/
    const JOB_VALIDATE_USERNAME = 1,
          JOB_VALIDATE_EMAIL    = 2;

    var userPo = _userPo;
    var confirmCallback = _confirmCallback;

    var remainingJobs = [];
    var counties;
    var errors;

    var role;
    var username;
    var email;
    var password;
    var county;

    var prevVal = {};

    // DOM elements
    var modal = $('#modal-edit-user-admin');
    modal.loadingMessage = modal.find('.loading');
    modal.form = modal.find('form');
    modal.form.roleDropdown = $('#meua-role');
    modal.form.roleDropdownDom = modal.form.roleDropdown[0];
    modal.form.username = $('#meua-username');
    modal.form.username.error = modal.form.username.parent().find('.invalid-feedback');
    modal.form.usernameDom = modal.form.username[0];
    modal.form.email = $('#meua-email');
    modal.form.email.error = modal.form.email.parent().find('.invalid-feedback');
    modal.form.emailDom = modal.form.email[0];
    modal.form.password = $('#meua-password');
    modal.form.countyDropdown = $('#meua-county');
    modal.form.countyDropdownDom = modal.form.countyDropdown[0];
    modal.form.saveButton = $('#meua-save');
    modal.form.saveButton.spinner = modal.form.saveButton.find('.spinner-border');

    modal.form.password.cls = new TogglePasswordField(modal.form.password);

    modal.form.username.regex = /^[a-zA-Z0-9_\-\.]+$/;
    modal.form.username.error.required = 'This field is required.';
    modal.form.username.error.regexMismatch = 'Username must be a string with a minimum of 1 character and a maximum of %maxlength% characters. Allowed characters include alphanumeric characters (lowercase and uppercase), dash, underscore and period.';
    modal.form.username.error.alreadyExists = 'Another user with name "%username%" already exists.';
    modal.form.email.error.required = modal.form.username.error.required;
    modal.form.email.error.invalid = 'Entered value is invalid.';
    modal.form.email.error.alreadyExists = 'Another user with e-mail "%email%" already exists.';

    bindEvents();
    startModalSetup();


    /*****************
     *    methods    *
     *****************/
    this.show = function () {
        showModal(modal);
    };


    /*******************
     *    functions    *
     *******************/
    function startModalSetup() {
        hideElementJquery(modal.form);
        showElementJquery(modal.loadingMessage);
        clearValidation();

        freezeSaveButton();

        /*
         * initialize roleDropdown - flush and repopulate with options
         * defined in Auth
         */
        modal.form.roleDropdown.empty();
        modal.form.roleDropdown.append(
            document.createElement('option')
        );

        var roles = auth.roleObjs();

        for (var i = 0; i < roles.length; i++) {
            var role = roles[i];

            var option = $(document.createElement('option'));
            option.prop('value', role.id);
            option.prop('selected', userPo.role === role.id);

            option.text(role.name);

            modal.form.roleDropdown.append(option);
        }

        modal.form.username.val(userPo.username);
        modal.form.email.val(userPo.email);
        modal.form.password.cls.clear();

        api.fetchCounties(countiesFetched);
    }

    function clearValidation() {
        modal.form.removeClass('was-validated');
        modal.form.usernameDom.setCustomValidity('');
        modal.form.emailDom.setCustomValidity('');
    }

    function filteredFieldChangeHandler(event) {
        setTimeout(delayedFieldChangeHandler, 0, event);
    }

    function delayedFieldChangeHandler(event) {
        var senderJq = $(event.currentTarget);
        var key = senderJq.attr('id');
        var senderValue = senderJq.val();

        var prevValIsUndefined = prevVal[key] === undefined;
        var newValIsEmpty = senderValue.length === 0;
        var notAChange = prevValIsUndefined && newValIsEmpty;

        var valuesDiffer = prevVal[key] !== senderValue;

        var different = !notAChange && valuesDiffer;

        if (different) {
            fieldChanged();
        }

        prevVal[key] = senderValue;
    }

    function fieldChanged() {
        clearValidation();
        unfreezeSaveButton();
    }

    function countiesFetched(success, _counties) {
        counties = _counties;

        finalizeModalSetup();
    }

    function finalizeModalSetup() {
        for (var i = 0; i < counties.length; i++) {
            var county = counties[i];

            var option = $(document.createElement('option'));
            option.prop('value', county.id);
            option.prop('selected', userPo.county.id === county.id);

            option.text(county.name);

            modal.form.countyDropdown.append(option);
        }

        hideElementJquery(modal.loadingMessage);
        showElementJquery(modal.form);

        unfreezeSaveButton();
    }

    function submitHandler(event) {
        event.preventDefault();
        saveConfirmed();
    }

    function freezeSaveButton() {
        disableButtonJquery(modal.form.saveButton);
    }

    function unfreezeSaveButton() {
        enableButtonJquery(modal.form.saveButton);
    }

    function showSpinner() {
        showElementJquery(modal.form.saveButton.spinner);
    }

    function hideSpinner() {
        hideElementJquery(modal.form.saveButton.spinner);
    }

    function saveConfirmed() {
        freezeSaveButton();
        showSpinner();

        role = modal.form.roleDropdown.find('option:selected').val();
        username = modal.form.username.val();
        email = modal.form.email.val();
        password = modal.form.password.val();
        county = parseInt(modal.form.countyDropdown.find('option:selected').val());

        startValidation();
    }

    function startValidation() {
        errors = 0;

        var roleDropdownBuiltinValid = modal.form.roleDropdownDom.checkValidity();
        if (!roleDropdownBuiltinValid) {
            errors++;
        }

        var usernameBuiltinValid = modal.form.usernameDom.checkValidity();
        var usernameRegexValid = modal.form.username.regex.test(username);
        if (!usernameBuiltinValid) {
            modal.form.username.error.text(modal.form.username.error.required);
            errors++;
        }
        else if (!usernameRegexValid) {
            var msg = modal.form.username.error.regexMismatch;
            var maxlength = modal.form.username.attr('maxlength');
            msg = msg.replace(/%maxlength%/g, maxlength);
            modal.form.username.error.text(msg);
            modal.form.usernameDom.setCustomValidity(msg);

            errors++;
        }
        else {
            remainingJobs.push(JOB_VALIDATE_USERNAME);

            var params = {
                q: username,
                exact: true,
                except: userPo.id
            };
            api.fetchUsers(
                usernameExists,
                $.param(params)
            );
        }

        var emailBuiltinValid = modal.form.emailDom.checkValidity();
        if (!emailBuiltinValid) {
            if (modal.form.emailDom.validity.valueMissing) {
                modal.form.email.error.text(modal.form.email.error.required);
            }
            else {
                modal.form.email.error.text(modal.form.email.error.invalid);
            }
            errors++;
        }
        else {
            remainingJobs.push(JOB_VALIDATE_EMAIL);

            var params = {
                email: email,
                exact: true,
                except: userPo.id
            };
            api.fetchUsers(
                emailExists,
                $.param(params)
            );
        }

        var countyDropdownBuiltinValid = modal.form.countyDropdownDom.checkValidity();
        if (!countyDropdownBuiltinValid) {
            errors++;
        }

        if (remainingJobs.length === 0) {
            validationFailed();
        }
    }

    function usernameExists(success, users) {
        var exists = users.length !== 0;

        remainingJobs = removeIntFromArray(remainingJobs, JOB_VALIDATE_USERNAME);

        if (exists) {
            var msg = modal.form.username.error.alreadyExists;
            msg = msg.replace(/%username%/g, username);
            modal.form.username.error.text(msg);
            modal.form.usernameDom.setCustomValidity(msg);

            errors++;
        }

        if (remainingJobs.length === 0) {
            if (errors !== 0) {
                validationFailed();
            }
            else {
                validationPassed();
            }
        }
    }

    function emailExists(success, usersWithEmail) {
        var exists = usersWithEmail.length !== 0;

        remainingJobs = removeIntFromArray(remainingJobs, JOB_VALIDATE_EMAIL);

        if (exists) {
            var msg = modal.form.email.error.alreadyExists;
            msg = msg.replace(/%email%/g, email);
            modal.form.email.error.text(msg);
            modal.form.emailDom.setCustomValidity(msg);

            errors++;
        }

        if (remainingJobs.length === 0) {
            if (errors !== 0) {
                validationFailed();
            }
            else {
                validationPassed();
            }
        }
    }

    function validationFailed() {
        modal.form.addClass('was-validated');
        hideSpinner();
    }

    function validationPassed() {
        var params = {
            role: role,
            username: username,
            email: email,
            county: county
        };

        if (password.length !== 0) {
            params.password = password;
        }

        api.updateUser(
            userPo.id,
            $.param(params),
            saveProcessed
        );
    }

    function saveProcessed() {
        hideModal(modal);
        confirmCallback();
    }

    function bindEvents() {
        modal.form.submit(submitHandler);

        modal.form.roleDropdown.change(fieldChanged);
        modal.form.username.on('change keydown cut paste', filteredFieldChangeHandler);
        modal.form.email.on('change keydown cut paste', filteredFieldChangeHandler);
        modal.form.password.on('change keydown cut paste', filteredFieldChangeHandler);
        modal.form.password.cls.bindEvents();
        modal.form.countyDropdown.change(fieldChanged);

        modal.on('hidden.bs.modal', hideHandler);
    }

    function hideHandler() {
        hideSpinner();
        unbindEvents();
    }

    function unbindEvents() {
        modal.form.off('submit', submitHandler);

        modal.form.roleDropdown.off('change', fieldChanged);
        modal.form.username.off('change keydown cut paste', filteredFieldChangeHandler);
        modal.form.email.off('change keydown cut paste', filteredFieldChangeHandler);
        modal.form.password.off('change keydown cut paste', filteredFieldChangeHandler);
        modal.form.password.cls.unbindEvents();
        modal.form.countyDropdown.off('change', fieldChanged);

        modal.off('hidden.bs.modal', hideHandler);
    }
}

function AFCategoriesTab() {
    /****************
     *    init()    *
     ****************/
    var categories = [];

    // DOM elements
    var tab = $('#af-categories');
    tab.addButton = tab.find('.add');
    tab.loadingMessage = tab.find('.loading');
    tab.table = tab.find('table');
    tab.table.tbody = tab.table.find('tbody');

    // event binding
    tab.addButton.click(showCategoryCreateModal);
    tab.table.tbody.on('click', '.edit', showCategoryEditModal);
    tab.table.tbody.on('click', '.delete', showCategoryDeleteModal);

    // Mustache templates
    var rowTemplate = $('#af-categories-table-row').html();
    Mustache.parse(rowTemplate);

    var emptyRowTemplate = '<tr><td class="text-center" colspan="2">No categories exist.</td></tr>';


    /*****************
     *    methods    *
     *****************/
    this.show = function () {
        startTableReload();
    };


    /*******************
     *    functions    *
     *******************/
    function populateTable() {
        tab.table.tbody.empty();

        for (var i = 0; i < categories.length; i++) {
            var category = categories[i];

            var row = Mustache.render(rowTemplate, {
                id: category.id,
                title: category.title
            });

            tab.table.tbody.append(row);
        }

        if (categories.length === 0) {
            tab.table.tbody.append(emptyRowTemplate);
        }

        hideElementJquery(tab.loadingMessage);
        showElementJquery(tab.table);
    }

    function startTableReload() {
        hideElementJquery(tab.table);
        showElementJquery(tab.loadingMessage);

        api.fetchCategories(categoriesFetched);
    }

    function categoriesFetched(success, _categories) {
        categories = _categories;

        populateTable();
    }

    function showCategoryCreateModal() {
        var modal = new CategoryCreateModal(startTableReload);
        modal.show();
    }

    function showCategoryEditModal(event) {
        var category = getCategoryById(getCategoryIdFromSenderEvent(event));

        var modal = new CategoryEditModal(category, startTableReload);
        modal.show();
    }

    function showCategoryDeleteModal(event) {
        var category = getCategoryById(getCategoryIdFromSenderEvent(event));

        var modal = new CategoryDeleteModal(category, startTableReload);
        modal.show();
    }

    function getCategoryIdFromSenderEvent(senderEvt) {
        var sender = $(senderEvt.currentTarget);
        var idHolder = sender.parent().parent().parent();
        var id = idHolder.data('id');

        return id;
    }

    function getCategoryById(id) {
        for (var i = 0; i < categories.length; i++) {
            var category = categories[i];
            if (id === category.id) {
                return category;
            }
        }
    }
}

function CategoryCreateModal(_confirmCallback) {
    /****************
     *    init()    *
     ****************/
    const ERR_REQUIRED      = 1,
          ERR_ALREADYEXISTS = 2;

    var confirmCallback = _confirmCallback;

    var title;

    var prevVal = {};

    // DOM elements
    var modal = $('#modal-create-category');
    modal.form = modal.find('form');
    modal.form.title = $('#mcca-title');
    modal.form.titleDom = modal.form.title[0];
    modal.form.title.error = modal.form.title.parent().find('.invalid-feedback');
    modal.form.createButton = $('#mcca-create');
    modal.form.createButton.spinner = modal.form.createButton.find('.spinner-border');

    bindEvents();
    setupModal();

    modal.form.title.error.defaultValue = 'This field is required.';
    modal.form.title.error.categoryAlreadyExists = 'Category "%title%" already exists.';


    /*****************
     *    methods    *
     *****************/
    this.show = function () {
        showModal(modal);
    };


    /*******************
     *    functions    *
     *******************/
    function setupModal() {
        flushTitle();
        clearValidation();
    }

    function flushTitle() {
        modal.form.title.val('');
        modal.form.title.change();
    }

    function clearValidation() {
        modal.form.removeClass('was-validated');
        modal.form.title.error.text(modal.form.title.error.defaultValue);
        modal.form.titleDom.setCustomValidity('');

        unfreezeCreateButton();
    }

    function filteredFieldChangeHandler(event) {
        setTimeout(delayedFieldChangeHandler, 0, event);
    }

    function delayedFieldChangeHandler(event) {
        var senderJq = $(event.currentTarget);
        var key = senderJq.attr('id');
        var senderValue = senderJq.val();

        var prevValIsUndefined = prevVal[key] === undefined;
        var newValIsEmpty = senderValue.length === 0;
        var notAChange = prevValIsUndefined && newValIsEmpty;

        var valuesDiffer = prevVal[key] !== senderValue;

        var different = !notAChange && valuesDiffer;

        if (different) {
            fieldChanged();
        }

        prevVal[key] = senderValue;
    }

    function fieldChanged() {
        clearValidation();
        unfreezeCreateButton();
    }

    function unfreezeCreateButton() {
        enableButtonJquery(modal.form.createButton);
    }

    function freezeCreateButton() {
        disableButtonJquery(modal.form.createButton);
    }

    function showSpinner() {
        showElementJquery(modal.form.createButton.spinner);
    }

    function hideSpinner() {
        hideElementJquery(modal.form.createButton.spinner);
    }

    function submitHandler(event) {
        event.preventDefault();
        createConfirmed();
    }

    function createConfirmed() {
        freezeCreateButton();
        showSpinner();

        validate();
    }

    function validate() {
        var builtinFieldsValid = modal.form[0].checkValidity();

        if (builtinFieldsValid) {
            title = modal.form.title.val();

            api.fetchCategories(categoriesFetched);
        }
        else {
            validationFailed(ERR_REQUIRED);
        }
    }

    function categoriesFetched(success, categories) {
        var exists = false;

        for (var i = 0; i < categories.length; i++) {
            var category = categories[i];
            if (category.title === title) {
                exists = true;
                break;
            }
        }

        if (!exists) {
            validationPassed();
        }
        else {
            validationFailed(ERR_ALREADYEXISTS);
        }
    }

    function validationFailed(errorType) {
        if (errorType === ERR_REQUIRED) {
            modal.form.title.error.text(modal.form.title.error.defaultValue);
        }
        else {
            var msg = modal.form.title.error.categoryAlreadyExists;
            msg = msg.replace(/%title%/g, title);
            modal.form.title.error.text(msg);
            modal.form.titleDom.setCustomValidity(msg);
        }
        modal.form.addClass('was-validated');

        hideSpinner();
    }

    function validationPassed() {
        var params = {
            title: title
        };

        api.storeCategory(
            $.param(params),
            createProcessed
        );
    }

    function createProcessed() {
        hideSpinner();
        hideModal(modal);
        confirmCallback();
    }

    function bindEvents() {
        modal.form.submit(submitHandler);
        modal.form.title.on('change keydown cut paste', filteredFieldChangeHandler);
        modal.form.createButton.click(createConfirmed);

        modal.on('hide.bs.modal', unbindEvents);
    }

    function unbindEvents() {
        modal.form.off('submit', submitHandler);
        modal.form.title.off('change keydown cut paste', filteredFieldChangeHandler);
        modal.form.createButton.off('click', createConfirmed);

        modal.off('hide.bs.modal', unbindEvents);
    }
}

function CategoryEditModal(_category, _confirmCallback) {
    /****************
     *    init()    *
     ****************/
    const ERR_REQUIRED      = 1,
          ERR_ALREADYEXISTS = 2;

    var category = _category;
    var confirmCallback = _confirmCallback;

    var title;

    // DOM elements
    var modal = $('#modal-edit-category');
    modal.form = modal.find('form');
    modal.form.title = $('#meca-title');
    modal.form.titleDom = modal.form.title[0];
    modal.form.title.error = modal.form.title.parent().find('.invalid-feedback');
    modal.form.saveButton = $('#meca-save');
    modal.form.saveButton.spinner = modal.form.saveButton.find('.spinner-border');

    bindEvents();
    setupModal();

    modal.form.title.error.defaultValue = 'This field is required.';
    modal.form.title.error.categoryAlreadyExists = 'Another category with title "%title%" already exists';


    /*****************
     *    methods    *
     *****************/
    this.show = function () {
        showModal(modal);
    };


    /*******************
     *    functions    *
     *******************/
    function setupModal() {
        setTitle(category.title);
        clearValidation();
    }

    function setTitle(title) {
        modal.form.title.val(title);
        modal.form.title.change();
    }

    function clearValidation() {
        modal.form.removeClass('was-validated');
        modal.form.title.error.text(modal.form.title.error.defaultValue);
        modal.form.titleDom.setCustomValidity('');

        unfreezeSaveButton();
    }

    function unfreezeSaveButton() {
        enableButtonJquery(modal.form.saveButton);
    }

    function freezeSaveButton() {
        disableButtonJquery(modal.form.saveButton);
    }

    function showSpinner() {
        showElementJquery(modal.form.saveButton.spinner);
    }

    function hideSpinner() {
        hideElementJquery(modal.form.saveButton.spinner);
    }

    function submitHandler(event) {
        event.preventDefault();
        saveConfirmed();
    }

    function saveConfirmed() {
        freezeSaveButton();
        showSpinner();

        validate();
    }

    function validate() {
        var builtinFieldsValid = modal.form[0].checkValidity();

        if (builtinFieldsValid) {
            title = modal.form.title.val();

            api.fetchCategories(categoriesFetched);
        }
        else {
            validationFailed(ERR_REQUIRED);
        }
    }

    function categoriesFetched(success, categories) {
        var exists = false;

        for (var i = 0; i < categories.length; i++) {
            var c = categories[i];

            var isNotCurrentlyEditedCategory = c.id !== category.id;
            var hasSameTitle = c.title === title;

            if (isNotCurrentlyEditedCategory && hasSameTitle) {
                exists = true;
                break;
            }
        }

        if (!exists) {
            validationPassed();
        }
        else {
            validationFailed(ERR_ALREADYEXISTS);
        }
    }

    function validationFailed(errorType) {
        if (errorType === ERR_REQUIRED) {
            modal.form.title.error.text(modal.form.title.error.defaultValue);
        }
        else {
            var msg = modal.form.title.error.categoryAlreadyExists;
            msg = msg.replace(/%title%/g, title);
            modal.form.title.error.text(msg);
            modal.form.titleDom.setCustomValidity(msg);
        }
        modal.form.addClass('was-validated');

        hideSpinner();
    }

    function validationPassed() {
        var params = {
            title: title
        };

        api.updateCategory(
            category.id,
            $.param(params),
            saveProcessed
        );
    }

    function saveProcessed() {
        hideSpinner();
        hideModal(modal);
        confirmCallback();
    }

    function bindEvents() {
        modal.form.submit(submitHandler);
        modal.form.title.on('change keydown cut paste', clearValidation);
        modal.form.saveButton.click(saveConfirmed);

        modal.on('hide.bs.modal', unbindEvents);
    }

    function unbindEvents() {
        modal.form.off('submit', submitHandler);
        modal.form.title.off('change keydown cut paste', clearValidation);
        modal.form.saveButton.off('click', saveConfirmed);

        modal.off('hide.bs.modal', unbindEvents);
    }
}

function CategoryDeleteModal(_category, _confirmCallback) {
    /****************
     *    init()    *
     ****************/
    var category = _category;
    var confirmCallback = _confirmCallback;

    // DOM elements
    var modal = $('#modal-delete-category');
    modal.titleLabel = modal.find('.title');
    modal.idLabel = modal.find('.id');
    modal.deleteButton = $('#mdca-delete');
    modal.deleteButton.spinner = modal.deleteButton.find('.spinner-border');

    bindEvents();
    setupModal();


    /*****************
     *    methods    *
     *****************/
    this.show = function () {
        showModal(modal);
    };


    /*******************
     *    functions    *
     *******************/
    function setupModal() {
        unfreezeDeleteButton();
        modal.titleLabel.text(category.title);
        modal.idLabel.text(category.id);
    }

    function unfreezeDeleteButton() {
        enableButtonJquery(modal.deleteButton);
    }

    function freezeDeleteButton() {
        disableButtonJquery(modal.deleteButton);
    }

    function showSpinner() {
        showElementJquery(modal.deleteButton.spinner);
    }

    function hideSpinner() {
        hideElementJquery(modal.deleteButton.spinner);
    }

    function deleteConfirmed() {
        freezeDeleteButton();
        showSpinner();

        api.destroyCategory(category.id, deleteProcessed);
    }

    function deleteProcessed() {
        hideSpinner();
        hideModal(modal);
        confirmCallback();
    }

    function bindEvents() {
        modal.deleteButton.click(deleteConfirmed);

        modal.on('hide.bs.modal', unbindEvents);
    }

    function unbindEvents() {
        modal.deleteButton.off('click', deleteConfirmed);

        modal.off('hide.bs.modal', unbindEvents);
    }
}

function AFCountiesTab() {
    /****************
     *    init()    *
     ****************/
    var counties = [];

    // DOM elements
    var tab = $('#af-counties');
    tab.addButton = tab.find('.add');
    tab.loadingMessage = tab.find('.loading');
    tab.table = tab.find('table');
    tab.table.tbody = tab.table.find('tbody');

    // event binding
    tab.addButton.click(showCountyCreateModal);
    tab.table.tbody.on('click', '.edit', showCountyEditModal);
    tab.table.tbody.on('click', '.delete', showCountyDeleteModal);

    // Mustache templates
    var rowTemplate = $('#af-counties-table-row').html();
    Mustache.parse(rowTemplate);

    var emptyRowTemplate = '<tr><td class="text-center" colspan="2">No counties exist.</td></tr>';


    /*****************
     *    methods    *
     *****************/
    this.show = function () {
        startTableReload();
    };


    /*******************
     *    functions    *
     *******************/
    function populateTable() {
        tab.table.tbody.empty();

        for (var i = 0; i < counties.length; i++) {
            var county = counties[i];

            var row = Mustache.render(rowTemplate, {
                id: county.id,
                name: county.name
            });

            tab.table.tbody.append(row);
        }

        if (counties.length === 0) {
            tab.table.tbody.append(emptyRowTemplate);
        }

        hideElementJquery(tab.loadingMessage);
        showElementJquery(tab.table);
    }

    function startTableReload() {
        hideElementJquery(tab.table);
        showElementJquery(tab.loadingMessage);

        api.fetchCounties(countiesFetched);
    }

    function countiesFetched(success, _counties) {
        counties = _counties;

        populateTable();
    }

    function showCountyCreateModal() {
        var modal = new CountyCreateModal(startTableReload);
        modal.show();
    }

    function showCountyEditModal(event) {
        var county = getCountyById(getCountyIdFromSenderEvent(event));

        var modal = new CountyEditModal(county, startTableReload);
        modal.show();
    }

    function showCountyDeleteModal(event) {
        var county = getCountyById(getCountyIdFromSenderEvent(event));

        var modal = new CountyDeleteModal(county, startTableReload);
        modal.show();
    }

    function getCountyIdFromSenderEvent(senderEvt) {
        var sender = $(senderEvt.currentTarget);
        var idHolder = sender.parent().parent().parent();
        var id = idHolder.data('id');

        return id;
    }

    function getCountyById(id) {
        for (var i = 0; i < counties.length; i++) {
            var county = counties[i];
            if (id === county.id) {
                return county;
            }
        }
    }
}

function CountyCreateModal(_confirmCallback) {
    /****************
     *    init()    *
     ****************/
    const ERR_REQUIRED      = 1,
          ERR_ALREADYEXISTS = 2;

    var confirmCallback = _confirmCallback;

    var name;

    // DOM elements
    var modal = $('#modal-create-county');
    modal.form = modal.find('form');
    modal.form.name = $('#mcco-name');
    modal.form.nameDom = modal.form.name[0];
    modal.form.name.error = modal.form.name.parent().find('.invalid-feedback');
    modal.form.createButton = $('#mcco-create');
    modal.form.createButton.spinner = modal.form.createButton.find('.spinner-border');

    bindEvents();
    setupModal();

    modal.form.name.error.defaultValue = 'This field is required.';
    modal.form.name.error.countyAlreadyExists = 'County "%name%" already exists.';


    /*****************
     *    methods    *
     *****************/
    this.show = function () {
        showModal(modal);
    };


    /*******************
     *    functions    *
     *******************/
    function setupModal() {
        flushName();
        clearValidation();
    }

    function flushName() {
        modal.form.name.val('');
        modal.form.name.change();
    }

    function clearValidation() {
        modal.form.removeClass('was-validated');
        modal.form.name.error.text(modal.form.name.error.defaultValue);
        modal.form.nameDom.setCustomValidity('');

        unfreezeCreateButton();
    }

    function unfreezeCreateButton() {
        enableButtonJquery(modal.form.createButton);
    }

    function freezeCreateButton() {
        disableButtonJquery(modal.form.createButton);
    }

    function showSpinner() {
        showElementJquery(modal.form.createButton.spinner);
    }

    function hideSpinner() {
        hideElementJquery(modal.form.createButton.spinner);
    }

    function submitHandler(event) {
        event.preventDefault();
        createConfirmed();
    }

    function createConfirmed() {
        freezeCreateButton();
        showSpinner();

        validate();
    }

    function validate() {
        var builtinFieldsValid = modal.form[0].checkValidity();

        if (builtinFieldsValid) {
            name = modal.form.name.val();

            api.fetchCounties(countiesFetched);
        }
        else {
            validationFailed(ERR_REQUIRED);
        }
    }

    function countiesFetched(success, counties) {
        var exists = false;

        for (var i = 0; i < counties.length; i++) {
            var county = counties[i];
            if (county.name === name) {
                exists = true;
                break;
            }
        }

        if (!exists) {
            validationPassed();
        }
        else {
            validationFailed(ERR_ALREADYEXISTS);
        }
    }

    function validationFailed(errorType) {
        if (errorType === ERR_REQUIRED) {
            modal.form.name.error.text(modal.form.name.error.defaultValue);
        }
        else {
            var msg = modal.form.name.error.countyAlreadyExists;
            msg = msg.replace(/%name%/g, name);
            modal.form.name.error.text(msg);
            modal.form.nameDom.setCustomValidity(msg);
        }
        modal.form.addClass('was-validated');

        hideSpinner();
    }

    function validationPassed() {
        var params = {
            name: name
        };

        api.storeCounty(
            $.param(params),
            createProcessed
        );
    }

    function createProcessed() {
        hideSpinner();
        hideModal(modal);
        confirmCallback();
    }

    function bindEvents() {
        modal.form.submit(submitHandler);
        modal.form.name.on('change keydown cut paste', clearValidation);
        modal.form.createButton.click(createConfirmed);

        modal.on('hide.bs.modal', unbindEvents);
    }

    function unbindEvents() {
        modal.form.off('submit', submitHandler);
        modal.form.name.off('change keydown cut paste', clearValidation);
        modal.form.createButton.off('click', createConfirmed);

        modal.off('hide.bs.modal', unbindEvents);
    }
}

function CountyEditModal(_county, _confirmCallback) {
    /****************
     *    init()    *
     ****************/
    const ERR_REQUIRED      = 1,
          ERR_ALREADYEXISTS = 2;

    var county = _county;
    var confirmCallback = _confirmCallback;

    var name;

    // DOM elements
    var modal = $('#modal-edit-county');
    modal.form = modal.find('form');
    modal.form.name = $('#meco-name');
    modal.form.nameDom = modal.form.name[0];
    modal.form.name.error = modal.form.name.parent().find('.invalid-feedback');
    modal.form.saveButton = $('#meco-save');
    modal.form.saveButton.spinner = modal.form.saveButton.find('.spinner-border');

    bindEvents();
    setupModal();

    modal.form.name.error.defaultValue = 'This field is required.';
    modal.form.name.error.countyAlreadyExists = 'Another county with name "%name%" already exists';


    /*****************
     *    methods    *
     *****************/
    this.show = function () {
        showModal(modal);
    };


    /*******************
     *    functions    *
     *******************/
    function setupModal() {
        setName(county.name);
        clearValidation();
    }

    function setName(name) {
        modal.form.name.val(name);
        modal.form.name.change();
    }

    function clearValidation() {
        modal.form.removeClass('was-validated');
        modal.form.name.error.text(modal.form.name.error.defaultValue);
        modal.form.nameDom.setCustomValidity('');

        unfreezeSaveButton();
    }

    function unfreezeSaveButton() {
        enableButtonJquery(modal.form.saveButton);
    }

    function freezeSaveButton() {
        disableButtonJquery(modal.form.saveButton);
    }

    function showSpinner() {
        showElementJquery(modal.form.saveButton.spinner);
    }

    function hideSpinner() {
        hideElementJquery(modal.form.saveButton.spinner);
    }

    function submitHandler(event) {
        event.preventDefault();
        saveConfirmed();
    }

    function saveConfirmed() {
        freezeSaveButton();
        showSpinner();

        validate();
    }

    function validate() {
        var builtinFieldsValid = modal.form[0].checkValidity();

        if (builtinFieldsValid) {
            name = modal.form.name.val();

            api.fetchCounties(countiesFetched);
        }
        else {
            validationFailed(ERR_REQUIRED);
        }
    }

    function countiesFetched(success, counties) {
        var exists = false;

        for (var i = 0; i < counties.length; i++) {
            var c = counties[i];

            var isNotCurrentlyEditedCounty = c.id !== county.id;
            var hasSameName = c.name === name;

            if (isNotCurrentlyEditedCounty && hasSameName) {
                exists = true;
                break;
            }
        }

        if (!exists) {
            validationPassed();
        }
        else {
            validationFailed(ERR_ALREADYEXISTS);
        }
    }

    function validationFailed(errorType) {
        if (errorType === ERR_REQUIRED) {
            modal.form.name.error.text(modal.form.name.error.defaultValue);
        }
        else {
            var msg = modal.form.name.error.countyAlreadyExists;
            msg = msg.replace(/%name%/g, name);
            modal.form.name.error.text(msg);
            modal.form.nameDom.setCustomValidity(msg);
        }
        modal.form.addClass('was-validated');

        hideSpinner();
    }

    function validationPassed() {
        var params = {
            name: name
        };

        api.updateCounty(
            county.id,
            $.param(params),
            saveProcessed
        );
    }

    function saveProcessed() {
        hideSpinner();
        hideModal(modal);
        confirmCallback();
    }

    function bindEvents() {
        modal.form.submit(submitHandler);
        modal.form.name.on('change keydown cut paste', clearValidation);
        modal.form.saveButton.click(saveConfirmed);

        modal.on('hide.bs.modal', unbindEvents);
    }

    function unbindEvents() {
        modal.form.off('submit', submitHandler);
        modal.form.name.off('change keydown cut paste', clearValidation);
        modal.form.saveButton.off('click', saveConfirmed);

        modal.off('hide.bs.modal', unbindEvents);
    }
}

function CountyDeleteModal(_county, _confirmCallback) {
    /****************
     *    init()    *
     ****************/
    var county = _county;
    var confirmCallback = _confirmCallback;

    // DOM elements
    var modal = $('#modal-delete-county');
    modal.nameLabel = modal.find('.name');
    modal.idLabel = modal.find('.id');
    modal.deleteButton = $('#mdco-delete');
    modal.deleteButton.spinner = modal.deleteButton.find('.spinner-border');

    bindEvents();
    setupModal();


    /*****************
     *    methods    *
     *****************/
    this.show = function () {
        showModal(modal);
    };


    /*******************
     *    functions    *
     *******************/
    function setupModal() {
        unfreezeDeleteButton();
        modal.nameLabel.text(county.name);
        modal.idLabel.text(county.id);
    }

    function unfreezeDeleteButton() {
        enableButtonJquery(modal.deleteButton);
    }

    function freezeDeleteButton() {
        disableButtonJquery(modal.deleteButton);
    }

    function showSpinner() {
        showElementJquery(modal.deleteButton.spinner);
    }

    function hideSpinner() {
        hideElementJquery(modal.deleteButton.spinner);
    }

    function deleteConfirmed() {
        freezeDeleteButton();
        showSpinner();

        api.destroyCounty(county.id, deleteProcessed);
    }

    function deleteProcessed() {
        hideSpinner();
        hideModal(modal);
        confirmCallback();
    }

    function bindEvents() {
        modal.deleteButton.click(deleteConfirmed);

        modal.on('hide.bs.modal', unbindEvents);
    }

    function unbindEvents() {
        modal.deleteButton.off('click', deleteConfirmed);

        modal.off('hide.bs.modal', unbindEvents);
    }
}

function ProfileFragment() {
    /****************
     *    init()    *
     ****************/
    var fragmentType = FragmentType.PROFILE;
    var loggedInOnly = false;
    var canHandleItems = true;

    var user;

    // DOM elements
    var container = $('#profile-fragment');
    var userQuery = container.find('.user');
    userQuery.label = userQuery.find('.username');
    userQuery.clear = userQuery.find('.clear');

    // event binding
    userQuery.clear.click(clearUserQuery);

    var classifiedGrid = new ClassifiedGrid(container);
    var filter = new Filter(container, false);


    /*****************
     *    methods    *
     *****************/
    this.hide = function () {
        hideElementJquery(container);
    };

    this.show = function (_user) {
        user = _user;
        userQuery.label.text(user.username());

        filter.show();

        showElementJquery(container);
    };

    this.login = function () {
        classifiedGrid.login();
        ui.search().run();
    };

    this.logout = function () {
        classifiedGrid.logout();
        ui.search().run();
    };

    this.prepareForItems = function () {
        filter.prepareForItems();
        classifiedGrid.prepareForItems();
    };

    this.setItems = function (items) {
        filter.setItems();
        classifiedGrid.setItems(items);
    };

    this.fragmentType = function () {
        return fragmentType;
    };

    this.loggedInOnly = function () {
        return loggedInOnly;
    };

    this.canHandleItems = function () {
        return canHandleItems;
    };


    /*******************
     *    functions    *
     *******************/
    function clearUserQuery() {
        ui.goTo(FragmentType.HOME, {});
    }
}

function Filter(container, _shouldShowCounty) {
    var shouldShowCounty = _shouldShowCounty;

    // DOM elements
    var query = container.find('.query');
    query.textQuery = query.find('.has-text-query');
    query.textQuery.value = query.textQuery.find('.text');
    query.textQuery.clear = query.textQuery.find('.clear');
    query.noTextQuery = query.find('.no-text-query');
    query.filters = query.find('.filter');
    query.filters.edit = query.filters.find('.edit');
    query.filters.clear = query.filters.find('.clear');

    // event binding
    query.textQuery.clear.click(clearTextQuery);
    query.filters.edit.click(showFiltersEditModal);
    query.filters.clear.click(clearFilters);


    /*****************
     *    methods    *
     *****************/
    this.show = function () {
        refresh();
    };

    this.prepareForItems = function () {
        hide();
    };

    this.setItems = function () {
        refresh();
        show();
    };


    /*******************
     *    functions    *
     *******************/
    function refresh() {
        refreshTextQuery();
        refreshFilterButtons();
    }

    function show() {
        showElementJquery(query);
    }

    function hide() {
        hideElementJquery(query);
    }

    function refreshTextQuery() {
        var textQuery = ui.search().textQuery();

        if (!!textQuery) {
            query.textQuery.value.text(textQuery);
            showElementJquery(query.textQuery);

            hideElementJquery(query.noTextQuery);
        }
        else {
            showElementJquery(query.noTextQuery);

            hideElementJquery(query.textQuery);
        }
    }

    function refreshFilterButtons() {
        var search = ui.search();
        var categoryIdFilter = search.categoryId();
        var countyIdFilter;

        if (shouldShowCounty) {
            countyIdFilter = search.countyId();
        }

        if (!!categoryIdFilter || !!countyIdFilter) {
            showElementJquery(query.filters.clear);
        }
        else {
            hideElementJquery(query.filters.clear);
        }
    }

    function clearTextQuery() {
        var search = ui.search();
        search.clearTextQuery();
        search.run();
    }

    function showFiltersEditModal() {
        var modal = new FiltersEditModal(shouldShowCounty);
        modal.show();
    }

    function clearFilters() {
        var search = ui.search();
        search.clearFilters();
        search.run();
    }
}

function FiltersEditModal(_shouldShowCounty) {
    /****************
     *    init()    *
     ****************/
    const JOB_FETCHCATEGORIES = 1,
          JOB_FETCHCOUNTIES = 2;

    var remainingJobs = [];
    var categories;
    var counties;

    var search = ui.search();

    var currentCategoryId = parseInt(search.categoryId());
    var currentCountyId = parseInt(search.countyId());

    var shouldShowCounty = _shouldShowCounty;

    // DOM elements
    var modal = $('#modal-filter');
    modal.form = modal.find('form');
    modal.form.categoryDropdown = $('#mf-category');
    modal.form.countyLabel = modal.find("label[for='mf-county']");
    modal.form.countyDropdown = $('#mf-county');
    modal.form.applyButton = $('#mf-apply');
    modal.loadingMessage = modal.find('.loading');

    bindEvents();
    startModalSetup();


    /*****************
     *    methods    *
     *****************/
    this.show = function () {
        showModal(modal);
    };


    /*******************
     *    functions    *
     *******************/
    function startModalSetup() {
        hideElementJquery(modal.form);
        showElementJquery(modal.loadingMessage);

        disableElementJquery(modal.form.applyButton);

        // initialize categoryDropdown
        modal.form.categoryDropdown.empty();
        modal.form.categoryDropdown.append(emptyOption());
        remainingJobs.push(JOB_FETCHCATEGORIES);
        api.fetchCategories(categoriesFetched);

        // initialize countyDropdown
        if (shouldShowCounty) {
            modal.form.countyDropdown.empty();
            modal.form.countyDropdown.append(emptyOption());
            showCountyElement();
            remainingJobs.push(JOB_FETCHCOUNTIES);
            api.fetchCounties(countiesFetched);
        }
        else {
            hideCountyElement();
        }
    }

    function emptyOption() {
        return document.createElement('option');
    }

    function hideCountyElement() {
        hideElementJquery(modal.form.countyLabel);
        hideElementJquery(modal.form.countyDropdown);
    }

    function showCountyElement() {
        showElementJquery(modal.form.countyLabel);
        showElementJquery(modal.form.countyDropdown);
    }

    function categoriesFetched(success, _categories) {
        categories = _categories;

        remainingJobs = removeIntFromArray(
            remainingJobs, JOB_FETCHCATEGORIES
        );

        if (remainingJobs.length === 0) {
            finalizeModalSetup();
        }
    }

    function countiesFetched(success, _counties) {
        counties = _counties;

        remainingJobs = removeIntFromArray(
            remainingJobs, JOB_FETCHCOUNTIES
        );

        if (remainingJobs.length === 0) {
            finalizeModalSetup();
        }
    }

    function finalizeModalSetup() {
        // loop variables
        var i, option;

        // categoryDropdown
        for (i = 0; i < categories.length; i++) {
            var category = categories[i];

            option = $(document.createElement('option'));
            option.prop('value', category.id);
            option.prop('selected', currentCategoryId === category.id);

            option.text(category.title);

            modal.form.categoryDropdown.append(option);
        }

        // countyDropdown
        if (shouldShowCounty) {
            for (i = 0; i < counties.length; i++) {
                var county = counties[i];

                option = $(document.createElement('option'));
                option.prop('value', county.id);
                option.prop('selected', currentCountyId === county.id);

                option.text(county.name);

                modal.form.countyDropdown.append(option);
            }
        }

        hideElementJquery(modal.loadingMessage);
        showElementJquery(modal.form);

        enableElementJquery(modal.form.applyButton);
    }

    function filtersConfirmed() {
        var changes = 0;

        var newCategoryId = parseInt(
            modal.form.categoryDropdown.find('option:selected').val()
        );

        if (isDifferent(currentCategoryId, newCategoryId)) {
            if (isNaN(newCategoryId)) {
                search.clearCategoryId();
            }
            else {
                search.setCategoryId(newCategoryId);
            }

            changes++;
        }

        if (shouldShowCounty) {
            var newCountyId = parseInt(
                modal.form.countyDropdown.find('option:selected').val()
            );

            if (isDifferent(currentCountyId, newCountyId)) {
                if (isNaN(newCountyId)) {
                    search.clearCountyId();
                }
                else {
                    search.setCountyId(newCountyId);
                }

                changes++;
            }
        }

        if (changes !== 0) {
            search.run();
        }

        hideModal(modal);
    }

    function isDifferent(lvalue, rvalue) {
        if (isNaN(lvalue) && isNaN(rvalue)) {
            return false;
        }
        else {
            return lvalue !== rvalue;
        }
    }

    function bindEvents() {
        modal.form.applyButton.click(filtersConfirmed);

        modal.on('hide.bs.modal', unbindEvents);
    }

    function unbindEvents() {
        modal.form.applyButton.off('click', filtersConfirmed);

        modal.off('hide.bs.modal', unbindEvents);
    }
}

function AccountSettingsFragment() {
    /****************
     *    init()    *
     ****************/
    const JOB_FETCHUSERDETAILS  = 1,
          JOB_FETCHCOUNTIES     = 2,
          JOB_VALIDATE_USERNAME = 3,
          JOB_VALIDATE_EMAIL    = 4;

    const PV_KEY_USERNAME = 'username',
          PV_KEY_EMAIL    =    'email',
          PV_KEY_PASSWORD = 'password';

    var fragmentType = FragmentType.ACCOUNT_SETTINGS;
    var loggedInOnly = true;
    var canHandleItems = false;

    var remainingJobs = [];
    var counties;
    var errors;

    var username;
    var email;
    var password;
    var county;

    var prevVal = {};

    // DOM elements
    var userUpdateSuccessToast = $('#toast-update-user-success');
    var container = $('#account-settings-fragment');
    var saveButton = container.find('.save');
    saveButton.spinner = saveButton.find('.spinner-border');
    var saveButtonMobile = container.find('.save-mobile');
    saveButtonMobile.spinner = saveButtonMobile.find('.spinner-border');
    var loadingMessage = container.find('.loading');
    var form = container.find('form');
    form.username = $('#asf-username');
    form.username.error = form.username.parent().find('.invalid-feedback');
    form.usernameDom = form.username[0];
    form.email = $('#asf-email');
    form.email.error = form.email.parent().find('.invalid-feedback');
    form.emailDom = form.email[0];
    form.password = $('#asf-password');
    form.countyDropdown = $('#asf-county');
    form.countyDropdownDom = form.countyDropdown[0];

    form.password.cls = new TogglePasswordField(form.password);

    // event binding
    form.submit(submitHandler);
    saveButton.click(saveConfirmed);
    saveButtonMobile.click(saveConfirmed);
    form.username.on('change keydown cut paste', null, PV_KEY_USERNAME, filteredFieldChangeHandler);
    form.email.on('change keydown cut paste', null, PV_KEY_EMAIL, filteredFieldChangeHandler);
    form.password.on('change keydown cut paste', null, PV_KEY_PASSWORD, filteredFieldChangeHandler);
    form.countyDropdown.on('change', fieldChanged);
    form.password.cls.bindEvents();

    form.username.error.required = 'This field is required.';
    form.username.error.usernameAlreadyTaken = 'Another user with name "%username%" already exists.';
    form.email.error.required = form.username.error.required;
    form.email.error.invalid = 'Entered value is invalid.';
    form.email.error.emailAlreadyTaken = 'Another user with e-mail "%email%" already exists.';


    /*****************
     *    methods    *
     *****************/
    this.hide = function () {
        hideElementJquery(container);
    };

    this.show = function () {
        startReload();
        showElementJquery(container);
    };

    this.fragmentType = function () {
        return fragmentType;
    };

    this.loggedInOnly = function () {
        return loggedInOnly;
    };

    this.canHandleItems = function () {
        return canHandleItems;
    };

    this.login = function () {};
    this.logout = function () {};
    this.prepareForItems = function () {};


    /*******************
     *    functions    *
     *******************/
    function startReload() {
        hideElementJquery(form);
        showElementJquery(loadingMessage);
        clearValidation();

        freezeSaveButton();

        form.password.cls.clear();

        form.countyDropdown.empty();
        var emptyOption = $(document.createElement('option'));
        form.countyDropdown.append(emptyOption);
        remainingJobs.push(JOB_FETCHCOUNTIES);
        api.fetchCounties(countiesFetched);

        remainingJobs.push(JOB_FETCHUSERDETAILS);
        api.getUser(auth.user().id(), userFetched);
    }

    function clearValidation() {
        form.removeClass('was-validated');
        form.usernameDom.setCustomValidity('');
        form.emailDom.setCustomValidity('');
    }

    function filteredFieldChangeHandler(event) {
        setTimeout(delayedFieldChangeHandler, 0, event);
    }

    function delayedFieldChangeHandler(event) {
        var key = event.data;
        var senderValue = $(event.currentTarget).val();

        if (prevVal[key] !== senderValue) {
            prevVal[key] = senderValue;

            fieldChanged();
        }
    }

    function fieldChanged() {
        clearValidation();
        unfreezeSaveButton();
    }

    function submitHandler(event) {
        event.preventDefault();
        saveConfirmed();
    }

    function countiesFetched(success, _counties) {
        counties = _counties;

        remainingJobs = removeIntFromArray(remainingJobs, JOB_FETCHCOUNTIES);

        if (remainingJobs.length === 0) {
            finalizeReload();
        }
    }

    function userFetched(success, userPo) {
        if (!success) {
            auth.logout();
            return;
        }

        auth.updateUserDetails(userPo);

        remainingJobs = removeIntFromArray(remainingJobs, JOB_FETCHUSERDETAILS);

        if (remainingJobs.length === 0) {
            finalizeReload();
        }
    }

    function finalizeReload() {
        var user = auth.user();
        form.username.val(user.username());
        form.email.val(user.email());

        for (var i = 0; i < counties.length; i++) {
            var county = counties[i];

            var option = $(document.createElement('option'));
            option.prop('value', county.id);

            // don't try to read 'id' if county itself is undefined
            if (!!user.county()) {
                option.prop('selected', user.county().id === county.id);
            }

            option.text(county.name);

            form.countyDropdown.append(option);
        }

        hideElementJquery(loadingMessage);
        showElementJquery(form);

        unfreezeSaveButton();
    }

    function freezeSaveButton() {
        disableButtonJquery(saveButton);
        disableButtonJquery(saveButtonMobile);
    }

    function unfreezeSaveButton() {
        enableButtonJquery(saveButton);
        enableButtonJquery(saveButtonMobile);
    }

    function showSpinner() {
        showElementJquery(saveButton.spinner);
        showElementJquery(saveButtonMobile.spinner);
    }

    function hideSpinner() {
        hideElementJquery(saveButton.spinner);
        hideElementJquery(saveButtonMobile.spinner);
    }

    function saveConfirmed() {
        freezeSaveButton();
        showSpinner();

        username = form.username.val();
        email = form.email.val();
        password = form.password.val();
        county = parseInt(form.countyDropdown.find('option:selected').val());

        startValidation();
    }

    function startValidation() {
        errors = 0;

        var usernameBuiltinValid = form.usernameDom.checkValidity();
        if (usernameBuiltinValid) {
            remainingJobs.push(JOB_VALIDATE_USERNAME);

            var params = {
                q: username,
                exact: true,
                except: auth.user().id()
            };
            api.fetchUsers(
                usernameExists,
                $.param(params)
            );
        }
        else {
            form.username.error.text(form.username.error.required);
            errors++;
        }

        var emailBuiltinValid = form.emailDom.checkValidity();
        if (emailBuiltinValid) {
            remainingJobs.push(JOB_VALIDATE_EMAIL);

            var params = {
                email: email,
                exact: true,
                except: auth.user().id()
            };
            api.fetchUsers(
                emailExists,
                $.param(params)
            );
        }
        else {
            if (form.emailDom.validity.valueMissing) {
                form.email.error.text(form.email.error.required);
            }
            else {
                form.email.error.text(form.email.error.invalid);
            }
            errors++;
        }

        if (!form.countyDropdownDom.checkValidity()) {
            errors++;
        }

        if (remainingJobs.length === 0) {
            /*
             * means was nothing pushed to the job queue, i.e. all
             * fields are invalid based on builtin rules
             */
            validationFailed();
        }
        // else there are async validation jobs
    }

    function usernameExists(success, users) {
        var exists = users.length !== 0;

        remainingJobs = removeIntFromArray(remainingJobs, JOB_VALIDATE_USERNAME);

        if (exists) {
            var msg = form.username.error.usernameAlreadyTaken;
            msg = msg.replace(/%username%/g, username);
            form.username.error.text(msg);
            form.usernameDom.setCustomValidity(msg);

            errors++;
        }

        if (remainingJobs.length === 0) {
            if (errors !== 0) {
                validationFailed();
            }
            else {
                validationPassed();
            }
        }
    }

    function emailExists(success, usersWithEmail) {
        var exists = usersWithEmail.length !== 0;

        remainingJobs = removeIntFromArray(remainingJobs, JOB_VALIDATE_EMAIL);

        if (exists) {
            var msg = form.email.error.emailAlreadyTaken;
            msg = msg.replace(/%email%/g, email);
            form.email.error.text(msg);
            form.emailDom.setCustomValidity(msg);

            errors++;
        }

        if (remainingJobs.length === 0) {
            if (errors !== 0) {
                validationFailed();
            }
            else {
                validationPassed();
            }
        }
    }

    function validationFailed() {
        form.addClass('was-validated');
        hideSpinner();
    }

    function validationPassed() {
        var params = {
            username: username,
            email: email,
            county: county
        };

        // don't update password if user didn't enter data in the field
        if (password.length !== 0) {
            params.password = password;
        }

        api.updateUser(
            auth.user().id(),
            $.param(params),
            saveProcessed
        );
    }

    function showSuccessToast() {
        userUpdateSuccessToast.toast('show');
    }

    function saveProcessed() {
        hideSpinner();
        startReload();
        showSuccessToast();
    }
}

function Navbar() {
    /****************
     *    init()    *
     ****************/
    var logo = $('nav .navbar-brand');
    logo.click(function () {ui.goTo(FragmentType.HOME, {senderIsLogo: true})});

    // guest UI elements
    var loginButton = $('#btn-login');

    // events
    loginButton.click(showLoginRegisterModal);

    // UI elements for users that are logged in
    var avatarDropdown = $('nav .avatar.dropdown');

    // items
    avatarDropdown.userItem = avatarDropdown.find('.user');
    avatarDropdown.adminItem = avatarDropdown.find('.admin');
    avatarDropdown.settingsItem = avatarDropdown.find('.settings');
    avatarDropdown.logoutItem = avatarDropdown.find('.logout');

    // item events
    avatarDropdown.userItem.click(function () {ui.goTo(FragmentType.PROFILE, {user: auth.user()})});
    avatarDropdown.adminItem.click(function () {ui.goTo(FragmentType.ADMIN, {})});
    avatarDropdown.settingsItem.click(function () {ui.goTo(FragmentType.ACCOUNT_SETTINGS, {})});
    avatarDropdown.logoutItem.click(auth.logout);

    // child elements
    avatarDropdown.userItem.username = avatarDropdown.userItem.find('span');


    /*****************
     *    methods    *
     *****************/
    this.logout = function () {
        /*
         * hide and disable all UI elements intended for the users
         * that are logged in
         */
        hideElementJquery(avatarDropdown);

        /* show all UI elements intended for guests */
        showElementJquery(loginButton);
    };

    this.login = function () {
        /*
         * opposite of setToLoggedOutState. hiding UI elements
         * intended for guests first
         */
        hideElementJquery(loginButton);

        /*
         * populate, show and enable UI elements intended for users
         * that are logged in
         */
        // TODO: set avatar image?
        avatarDropdown.userItem.username.text(auth.user().username());

        if (auth.user().isAdmin()) {
            showElementJquery(avatarDropdown.adminItem);
        }
        else {
            hideElementJquery(avatarDropdown.adminItem);
        }

        showElementJquery(avatarDropdown);
    };


    /*******************
     *    functions    *
     *******************/
    function showLoginRegisterModal() {
        var modal = new LoginRegisterModal();
        modal.show();
    }
}

function Search() {
    /****************
     *    init()    *
     ****************/
    const PARAM_KEY_TEXTQUERY = 'q',
          PARAM_KEY_FILTERS_CATEGORYID = 'category',
          PARAM_KEY_FILTERS_COUNTYID = 'county',
          PARAM_KEY_USERID = 'user';

    var params = {};
    var busy = false;

    // DOM elements
    var searchBox = $('#search-box');
    searchBox.formParent = $('.navbar form');
    searchBox.field = searchBox.find('input');
    searchBox.spinner = searchBox.find('.spinner-border');
    searchBox.searchButton = searchBox.find('button');

    // event binding
    searchBox.formParent.submit(searchFormSubmitted);
    searchBox.searchButton.click(searchQuerySubmitted);


    /*****************
     *    methods    *
     *****************/
    this.textQuery = function () {
        return params[PARAM_KEY_TEXTQUERY];
    };

    this.categoryId = function () {
        return params[PARAM_KEY_FILTERS_CATEGORYID];
    };

    this.countyId = function () {
        return params[PARAM_KEY_FILTERS_COUNTYID];
    };

    this.setCategoryId = function (categoryId) {
        params[PARAM_KEY_FILTERS_CATEGORYID] = categoryId;
    };

    this.setCountyId = function (countyId) {
        params[PARAM_KEY_FILTERS_COUNTYID] = countyId;
    };

    this.setUserId = function (userId) {
        params[PARAM_KEY_USERID] = userId;
    };

    this.clearAll = function () {
        clearTextQuery();
        clearFilters();
        clearUserId();
    };

    this.clearTextQuery = clearTextQuery;
    this.clearCategoryId = clearCategoryId;
    this.clearCountyId = clearCountyId;
    this.clearUserId = clearUserId;
    this.clearFilters = clearFilters;
    this.run = run;


    /*******************
     *    functions    *
     *******************/
    function clearTextQuery() {
        delete params[PARAM_KEY_TEXTQUERY];
        searchBox.field.val('');
    }

    function clearFilters() {
        clearCategoryId();
        clearCountyId();
    }

    function clearCategoryId() {
        delete params[PARAM_KEY_FILTERS_CATEGORYID];
    }

    function clearCountyId() {
        delete params[PARAM_KEY_FILTERS_COUNTYID];
    }

    function clearUserId() {
        delete params[PARAM_KEY_USERID];
    }

    function setTextQuery(query) {
        params[PARAM_KEY_TEXTQUERY] = query;
    }

    function showSearchButton() {
        showElementJquery(searchBox.searchButton);
    }

    function hideSearchButton() {
        hideElementJquery(searchBox.searchButton);
    }

    function showSpinner() {
        showElementJquery(searchBox.spinner);
    }

    function hideSpinner() {
        hideElementJquery(searchBox.spinner);
    }

    function searchFormSubmitted(event) {
        event.preventDefault();
        searchQuerySubmitted();
    }

    function searchQuerySubmitted() {
        // don't take new queries if one is already underway
        if (busy) { return; }

        var fieldValue = searchBox.field.val();

        if (fieldValue !== params[PARAM_KEY_TEXTQUERY]) {
            setTextQuery(fieldValue);
            run();
        }
    }

    function run() {
        busy = true;

        if (ui.fragmentCanHandleItems()) {
            ui.prepareFragmentForItems();
        }
        else {
            hideSearchButton();
            showSpinner();
        }

        api.fetchClassifieds($.param(params), classifiedsRetrieved);
    }

    function classifiedsRetrieved(success, results) {
        hideSpinner();
        showSearchButton();
        ui.setItems(results);

        busy = false;
    }
}

function initCharacterCounter() {
    $('.form-group').characterCounter();
}

/*************
 *    api    *
 *************/
function initApi() {
    api = new Api();
}

function Api() {
    /****************
     *    init()    *
     ****************/
    const ENDPOINT_IMAGE_STORE =        '/api/images/',
          ENDPOINT_AUTH_LOGIN =         '/api/auth/login/',
          ENDPOINT_AUTH_REGISTER =      '/api/auth/register/',
          ENDPOINT_USER_GET =           '/api/users/%id%/',
          ENDPOINT_USERS_INDEX =        '/api/users/',
          ENDPOINT_USER_UPDATE =        '/api/users/%id%/',
          ENDPOINT_USER_DESTROY =       '/api/users/%id%/',
          ENDPOINT_CATEGORY_STORE =     '/api/categories/',
          ENDPOINT_CATEGORIES_INDEX =   '/api/categories/',
          ENDPOINT_CATEGORY_UPDATE =    '/api/categories/%id%/',
          ENDPOINT_CATEGORY_DESTROY =   '/api/categories/%id%/',
          ENDPOINT_COUNTY_STORE =       '/api/counties/',
          ENDPOINT_COUNTIES_INDEX =     '/api/counties/',
          ENDPOINT_COUNTY_UPDATE =      '/api/counties/%id%/',
          ENDPOINT_COUNTY_DESTROY =     '/api/counties/%id%/',
          ENDPOINT_CLASSIFIED_STORE =   '/api/classifieds/',
          ENDPOINT_CLASSIFIEDS_INDEX =  '/api/classifieds/',
          ENDPOINT_CLASSIFIED_UPDATE =  '/api/classifieds/%id%/',
          ENDPOINT_CLASSIFIED_DESTROY = '/api/classifieds/%id%/';

    const JOBID_CHARS = 8;

    const JOB_AUTH_LOGIN         =  1,
          JOB_AUTH_REGISTER      =  2,
          JOB_USER_GET           =  3,
          JOB_USERS_INDEX        =  4,
          JOB_USER_UPDATE        =  5,
          JOB_USER_DESTROY       =  6,
          JOB_CATEGORY_STORE     =  7,
          JOB_CATEGORIES_INDEX   =  8,
          JOB_CATEGORY_UPDATE    =  9,
          JOB_CATEGORY_DESTROY   = 10,
          JOB_COUNTY_STORE       = 11,
          JOB_COUNTIES_INDEX     = 12,
          JOB_COUNTY_UPDATE      = 13,
          JOB_COUNTY_DESTROY     = 14,
          JOB_CLASSIFIED_STORE   = 15,
          JOB_CLASSIFIEDS_INDEX  = 16,
          JOB_CLASSIFIED_UPDATE  = 17,
          JOB_CLASSIFIED_DESTROY = 18;

    const _DEBUG_CB_TIMEOUT_MS = 3000;

    const _DEBUG_MOCK_USERLIST = [
        {
            id: 0,
            username: 'adminuser',
            email: 'admin@vand.com',
            role: 1,
            county: {
                id: 1,
                name: 'Bjelovarsko-bilogorska'
            }
        },
        {
            id: 1,
            username: 'useruser',
            email: 'userman@hmail.com',
            role: 0,
            county: {
                id: 2,
                name: 'Osjecko-baranjska'
            }
        },
        {
            id: 2,
            username: 'exampleuser',
            email: 'exampleuser007@email.com',
            role: 0,
            county: {
                id: 4,
                name: 'Viroviticko-podravska'
            }
        },
        {
            id: 3,
            username: 'sampleuser',
            email: 'sampleuser@bmail.com',
            role: 0,
            county: {
                id: 3,
                name: 'Pozesko-slavonska'
            }
        },
        {
            id: 4,
            username: 'anotheruser2',
            email: 'anotheruser2@cmail.com',
            role: 0,
            county: {
                id: 3,
                name: 'Pozesko-slavonska'
            }
        },
        {
            id: 5,
            username: 'anotheradminuser',
            email: 'anotheradminuser@vand.com',
            role: 1,
            county: {
                id: 4,
                name: 'Viroviticko-podravska'
            }
        }
    ];

    const _DEBUG_MOCK_CLASSIFIEDSLIST = [
        {
            id: 1,
            title: 'Cookbook',
            description: 'Awesome cookbook.',
            price: 5,
            category: {
                id: 2,
                title: 'Books'
            },
            images: [{
                id: 1,
                url: 'https://mdbootstrap.com/img/Photos/Others/food3.jpg'
            }],
            seller: {
                id: 0,
                username: 'adminuser',
                email: 'admin@vand.com',
                role: 1,
                county: {
                    id: 1,
                    name: 'Bjelovarsko-bilogorska'
                }
            }
        },
        {
            id: 2,
            title: 'Cookbook with weird oranges',
            description: 'Bought this cookbook. It has these strange oranges, so I am not interested in it anymore.',
            price: 200,
            category: {
                id: 2,
                title: 'Books'
            },
            images: [{
                id: 2,
                url: 'https://mdbootstrap.com/img/Photos/Others/food5.jpg'
            }],
            seller: {
                id: 1,
                username: 'useruser',
                email: 'userman@hmail.com',
                role: 0,
                county: {
                    id: 2,
                    name: 'Osjecko-baranjska'
                }
            }
        },
        {
            id: 3,
            title: 'No image test',
            description: 'Just a test item with a broken image.',
            price: 0,
            category: {
                id: 3,
                title: 'Computers and tablets'
            },
            images: [{
                id: 5,
                url: 'https://mdbootstrap.com/img/Photos/Others/foodNoImage.jpg'
            }],
            seller: {
                id: 2,
                username: 'exampleuser',
                email: 'exampleuser007@email.com',
                role: 0,
                county: {
                    id: 4,
                    name: 'Viroviticko-podravska'
                }
            }
        },
        {
            id: 4,
            title: '400x600 image test',
            description: 'Test item with 400x600 image.',
            price: 0,
            category: {
                id: 3,
                title: 'Computers and tablets'
            },
            images: [{
                id: 6,
                url: 'https://via.placeholder.com/400x600'
            }],
            seller: {
                id: 0,
                username: 'adminuser',
                email: 'admin@vand.com',
                role: 1,
                county: {
                    id: 1,
                    name: 'Bjelovarsko-bilogorska'
                }
            }
        },
        {
            id: 7,
            title: '600x400 image test',
            description: 'Test item with 600x400 image.',
            price: 0,
            category: {
                id: 3,
                title: 'Computers and tablets'
            },
            images: [{
                id: 7,
                url: 'https://via.placeholder.com/600x400'
            }],
            seller: {
                id: 1,
                username: 'useruser',
                email: 'userman@hmail.com',
                role: 0,
                county: {
                    id: 2,
                    name: 'Osjecko-baranjska'
                }
            }
        },
        {
            id: 5,
            title: 'Strange mixed breed feathery dog',
            description: 'Asked for a dog, and we ended up with this odd bird (pun NOT intended, I swear). \r\nUsed, low mileage. Barks, but also lets out some mighty weird noises. You can play fetch with him, but sometimes our things end up on Antarctica, so we tend to play only with biodegradable things. \n\nSold as-is!',
            price: 2000000000,
            category: {
                id: 1,
                title: 'Audio gear'
            },
            images: [
                {
                    id: 3,
                    url: 'https://mdbootstrap.com/img/Photos/Others/image006.jpg'
                },
                {
                    id: 8,
                    url: 'https://mdbootstrap.com/img/Photos/Slides/img%20(130).jpg'
                },
                {
                    id: 9,
                    url: 'https://via.placeholder.com/150'
                },
                {
                    id: 10,
                    url: 'https://via.placeholder.com/400x600'
                },
                {
                    id: 11,
                    url: 'https://mdbootstrap.com/img/Photos/Slides/img%20(70).jpg'
                }
            ],
            seller: {
                id: 2,
                username: 'exampleuser',
                email: 'exampleuser007@email.com',
                role: 0,
                county: {
                    id: 4,
                    name: 'Viroviticko-podravska'
                }
            }
        },
        {
            id: 6,
            title: 'Nice picture',
            description: 'Really, really nice picture. Blew my mind!',
            price: 2000000000,
            category: {
                id: 2,
                title: 'Books'
            },
            images: [{
                id: 4,
                url: 'https://mdbootstrap.com/img/Photos/Others/image007.jpg'
            }],
            seller: {
                id: 0,
                username: 'adminuser',
                email: 'admin@vand.com',
                role: 1,
                county: {
                    id: 1,
                    name: 'Bjelovarsko-bilogorska'
                }
            }
        },
        {
            id: 8,
            title: 'Tiny picture',
            description: 'Tiny picture of a bear. And some cat tax, just in case you are not that much into bears.',
            price: 200,
            category: {
                id: 2,
                title: 'Books'
            },
            images: [
                {
                    id: 12,
                    url: 'https://placebear.com/80/80'
                },
                {
                    id: 15,
                    url: 'https://placekitten.com/80/80'
                }
            ],
            seller: {
                id: 0,
                username: 'adminuser',
                email: 'admin@vand.com',
                role: 1,
                county: {
                    id: 1,
                    name: 'Bjelovarsko-bilogorska'
                }
            }
        }
    ];

    const _DEBUG_MOCK_CATEGORIESLIST = [
        {
            id: 1,
            title: 'Audio gear'
        },
        {
            id: 2,
            title: 'Books'
        },
        {
            id: 3,
            title: 'Computers and tablets'
        }
    ];

    const _DEBUG_MOCK_COUNTIESLIST = [
        {
            id: 1,
            name: 'Bjelovarsko-bilogorska'
        },
        {
            id: 2,
            name: 'Osjecko-baranjska'
        },
        {
            id: 3,
            name: 'Pozesko-slavonska'
        },
        {
            id: 4,
            name: 'Viroviticko-podravska'
        }
    ];

    var pendingJobs = [];


    /*****************
     *    methods    *
     *****************/
    this.imageStoreEndpoint = function () {
        return ENDPOINT_IMAGE_STORE;
    };

    this.login = function (formData, callback) {
        var jobId = genId();
        var jobType = JOB_AUTH_LOGIN;

        var xhr = $.ajax({
            method: "POST",
            url: ENDPOINT_AUTH_LOGIN,
            data: formData,
            success: function (d) { jobCompleted(jobId, callback, d); },
            error: function (d) { jobFailed(jobId, callback, d); }
        });

        addJob(
            jobId,
            jobType,
            xhr
        );
    };

    this.register = function (formData, callback) {
        var jobId = genId();
        var jobType = JOB_AUTH_REGISTER;

        var xhr = $.ajax({
            method: "POST",
            url: ENDPOINT_AUTH_REGISTER,
            data: formData,
            success: function (d) { jobCompleted(jobId, callback, d); },
            error: function (d) { jobFailed(jobId, callback, d); }
        });

        addJob(
            jobId,
            jobType,
            xhr
        );
    };

    this.storeClassified = function (formData, callback) {
        var jobId = genId();
        var jobType = JOB_CLASSIFIED_STORE;

        var xhr = $.ajax({
            method: "POST",
            url: ENDPOINT_CLASSIFIED_STORE,
            headers: authTokenHeader(),
            data: formData,
            success: function (d) { jobCompleted(jobId, callback, d); },
            error: function (d) { jobFailed(jobId, callback, d); }
        });

        addJob(
            jobId,
            jobType,
            xhr
        );
    };

    this.storeCategory = function (formData, callback) {
        var jobId = genId();
        var jobType = JOB_CATEGORY_STORE;

        var xhr = $.ajax({
            method: "POST",
            url: ENDPOINT_CATEGORY_STORE,
            headers: authTokenHeader(),
            data: formData,
            success: function (d) { jobCompleted(jobId, callback, d); },
            error: function (d) { jobFailed(jobId, callback, d); }
        });

        addJob(
            jobId,
            jobType,
            xhr
        );
    };

    this.storeCounty = function (formData, callback) {
        var jobId = genId();
        var jobType = JOB_COUNTY_STORE;

        var xhr = $.ajax({
            method: "POST",
            url: ENDPOINT_COUNTY_STORE,
            headers: authTokenHeader(),
            data: formData,
            success: function (d) { jobCompleted(jobId, callback, d); },
            error: function (d) { jobFailed(jobId, callback, d); }
        });

        addJob(
            jobId,
            jobType,
            xhr
        );
    };

    this.fetchUsers = function (callback, formData) {
        var jobId = genId();
        var jobType = JOB_USERS_INDEX;

        var xhr = $.ajax({
            method: "GET",
            url: ENDPOINT_USERS_INDEX,
            data: formData,
            success: function (d) { jobCompleted(jobId, callback, d); },
            error: function (d) { jobFailed(jobId, callback, d); }
        });

        addJob(
            jobId,
            jobType,
            xhr
        );
    };

    this.getUser = function (id, callback) {
        var jobId = genId();
        var jobType = JOB_USER_GET;

        var url = ENDPOINT_USER_GET.replace(/%id%/g, id);

        var xhr = $.ajax({
            method: "GET",
            url: url,
            success: function (d) { jobCompleted(jobId, callback, d); },
            error: function (d) { jobFailed(jobId, callback, d); }
        });

        addJob(
            jobId,
            jobType,
            xhr
        );
    };

    this.destroyUser = function (id, callback) {
        var jobId = genId();
        var jobType = JOB_USER_DESTROY;

        var url = ENDPOINT_USER_DESTROY.replace(/%id%/g, id);

        var xhr = $.ajax({
            method: "DELETE",
            url: url,
            headers: authTokenHeader(),
            success: function (d) { jobCompleted(jobId, callback, d); },
            error: function (d) { jobFailed(jobId, callback, d); }
        });

        addJob(
            jobId,
            jobType,
            xhr
        );
    };

    this.destroyClassified = function (id, callback) {
        var jobId = genId();
        var jobType = JOB_CLASSIFIED_DESTROY;

        var url = ENDPOINT_CLASSIFIED_DESTROY.replace(/%id%/g, id);

        var xhr = $.ajax({
            method: "DELETE",
            url: url,
            headers: authTokenHeader(),
            success: function (d) { jobCompleted(jobId, callback, d); },
            error: function (d) { jobFailed(jobId, callback, d); }
        });

        addJob(
            jobId,
            jobType,
            xhr
        );
    };

    this.destroyCategory = function (id, callback) {
        var jobId = genId();
        var jobType = JOB_CATEGORY_DESTROY;

        var url = ENDPOINT_CATEGORY_DESTROY.replace(/%id%/g, id);

        var xhr = $.ajax({
            method: "DELETE",
            url: url,
            headers: authTokenHeader(),
            success: function (d) { jobCompleted(jobId, callback, d); },
            error: function (d) { jobFailed(jobId, callback, d); }
        });

        addJob(
            jobId,
            jobType,
            xhr
        );
    };

    this.destroyCounty = function (id, callback) {
        var jobId = genId();
        var jobType = JOB_COUNTY_DESTROY;

        var url = ENDPOINT_COUNTY_DESTROY.replace(/%id%/g, id);

        var xhr = $.ajax({
            method: "DELETE",
            url: url,
            headers: authTokenHeader(),
            success: function (d) { jobCompleted(jobId, callback, d); },
            error: function (d) { jobFailed(jobId, callback, d); }
        });

        addJob(
            jobId,
            jobType,
            xhr
        );
    };

    this.updateUser = function (id, formData, callback) {
        var jobId = genId();
        var jobType = JOB_USER_UPDATE;

        var url = ENDPOINT_USER_UPDATE.replace(/%id%/g, id);

        var xhr = $.ajax({
            method: "PUT",
            url: url,
            headers: authTokenHeader(),
            data: formData,
            success: function (d) { jobCompleted(jobId, callback, d); },
            error: function (d) { jobFailed(jobId, callback, d); }
        });

        addJob(
            jobId,
            jobType,
            xhr
        );
    };

    this.updateCounty = function (id, formData, callback) {
        var jobId = genId();
        var jobType = JOB_COUNTY_UPDATE;

        var url = ENDPOINT_COUNTY_UPDATE.replace(/%id%/g, id);

        var xhr = $.ajax({
            method: "PUT",
            url: url,
            headers: authTokenHeader(),
            data: formData,
            success: function (d) { jobCompleted(jobId, callback, d); },
            error: function (d) { jobFailed(jobId, callback, d); }
        });

        addJob(
            jobId,
            jobType,
            xhr
        );
    };

    this.updateCategory = function (id, formData, callback) {
        var jobId = genId();
        var jobType = JOB_CATEGORY_UPDATE;

        var url = ENDPOINT_CATEGORY_UPDATE.replace(/%id%/g, id);

        var xhr = $.ajax({
            method: "PUT",
            url: url,
            headers: authTokenHeader(),
            data: formData,
            success: function (d) { jobCompleted(jobId, callback, d); },
            error: function (d) { jobFailed(jobId, callback, d); }
        });

        addJob(
            jobId,
            jobType,
            xhr
        );
    };

    this.updateClassified = function (id, formData, callback) {
        var jobId = genId();
        var jobType = JOB_CLASSIFIED_UPDATE;

        var url = ENDPOINT_CLASSIFIED_UPDATE.replace(/%id%/g, id);

        var xhr = $.ajax({
            method: "PUT",
            url: url,
            headers: authTokenHeader(),
            data: formData,
            success: function (d) { jobCompleted(jobId, callback, d); },
            error: function (d) { jobFailed(jobId, callback, d); }
        });

        addJob(
            jobId,
            jobType,
            xhr
        );
    };

    this.fetchClassifieds = function (formData, callback) {
        var jobId = genId();
        var jobType = JOB_CLASSIFIEDS_INDEX;

        var xhr = $.ajax({
            method: "GET",
            url: ENDPOINT_CLASSIFIEDS_INDEX,
            data: formData,
            success: function (d) { jobCompleted(jobId, callback, d); },
            error: function (d) { jobFailed(jobId, callback, d); }
        });

        addJob(
            jobId,
            jobType,
            xhr
        );
    };

    this.fetchCategories = function (callback) {
        var jobId = genId();
        var jobType = JOB_CATEGORIES_INDEX;

        var xhr = $.ajax({
            method: "GET",
            url: ENDPOINT_CATEGORIES_INDEX,
            success: function (d) { jobCompleted(jobId, callback, d); },
            error: function (d) { jobFailed(jobId, callback, d); }
        });

        addJob(
            jobId,
            jobType,
            xhr
        );
    };

    this.fetchCounties = function (callback) {
        var jobId = genId();
        var jobType = JOB_COUNTIES_INDEX;

        var xhr = $.ajax({
            method: "GET",
            url: ENDPOINT_COUNTIES_INDEX,
            success: function (d) { jobCompleted(jobId, callback, d); },
            error: function (d) { jobFailed(jobId, callback, d); }
        });

        addJob(
            jobId,
            jobType,
            xhr
        );
    };

    this.cancelPendingRequests = function () {
        console.log({
            msg: 'cancelPendingRequests called',
            jobs: _DEBUG_GET_PENDINGJOBS_W_JOBTYPECVT()
        }); ///

        while (pendingJobs.length !== 0) {
            var job = pendingJobs[0];
            cancelJob(job.obj);
            removeJobById(job.id);
        }
    };


    /*******************
     *    functions    *
     *******************/
    function addJob(jobId, jobType, jobObj) {
        console.log({
            func: 'addJob',
            jobId: jobId,
            jobType: _DEBUG_CVT_JOBTYPE_TO_STR(jobType),
            jobObj: jobObj,
            jobs: _DEBUG_GET_PENDINGJOBS_W_JOBTYPECVT()
        }); ///

        pendingJobs.push({
            id: jobId,
            type: jobType,
            obj: jobObj
        });
    }

    function genId() {
        return randomNChars(JOBID_CHARS);
    }

    function removeJobById(jobId) {
        pendingJobs = pendingJobs.filter(
            function (job) { return job.id !== jobId; }
        );
    }

    function cancelJob(jobObj) {
        /*
         * TODO: [REST] remove clearTimeout section when all setTimeout calls
         * are replaced with $.ajax calls
         */
        if (jobObj.done && jobObj.fail && jobObj.always) {
            /*
             * it's a jQuery AJAX object
             * credits: https://stackoverflow.com/a/35134914
             */
            jobObj.abort();
        }
        else {
            // it's a setTimeout timer
            clearTimeout(jobObj);
        }
    }

    function jobCompleted(jobId, callback, response) {
        console.log({
            func: 'jobCompleted',
            jobId: jobId,
            jobType: _DEBUG_CVT_JOBTYPE_TO_STR(_DEBUG_GET_JOBTYPE_FROM_ID(jobId)),
            jobs: _DEBUG_GET_PENDINGJOBS_W_JOBTYPECVT(),
            response: response
        });

        removeJobById(jobId);

        if (!!response) {
            callback(true, response);
        }
        else {
            callback(true);
        }
    }

    function jobFailed(jobId, callback, response) {
        console.log({
            func: 'jobFailed',
            jobId: jobId,
            jobType: _DEBUG_CVT_JOBTYPE_TO_STR(_DEBUG_GET_JOBTYPE_FROM_ID(jobId)),
            jobs: _DEBUG_GET_PENDINGJOBS_W_JOBTYPECVT(),
            response: response
        });

        var aborted = response.status === 0 && response.statusText === 'abort';

        /*
         * if job was aborted, cancelPendingRequests has already removed it
         * from job queue, so no need to call removeJobById. executing
         * callback is not necessary either, as the caller no longer exists or
         * no longer needs data that was to be retrieved with this request.
         */
        if (aborted) {
            return;
        }

        removeJobById(jobId);

        var unauthorized = response.status === 401 &&
                           response.statusText === 'Unauthorized';
        // token expired or was never valid, logout
        if (unauthorized) {
            auth.logout();
        }

        var r = response.responseJSON;
        callback(false, r);
    }

    function authTokenHeader() {
        return {
            'Authorization': 'Token %token%'.replace(/%token%/g, auth.token())
        };
    }

    function _DEBUG_GET_PENDINGJOBS_W_JOBTYPECVT() {
        var jobs = [];

        for (var i = 0; i < pendingJobs.length; i++) {
            var pj = pendingJobs[i];

            jobs.push({
                id: pj.id,
                type: pj.type,
                typeDecoded: _DEBUG_CVT_JOBTYPE_TO_STR(pj.type),
                obj: pj.obj
            });
        }

        return jobs;
    }

    function _DEBUG_GET_JOBTYPE_FROM_ID(jobId) {
        for (var i = 0; i < pendingJobs.length; i++) {
            var job = pendingJobs[i];
            if (job.id === jobId) {
                return job.type;
            }
        }
    }

    function _DEBUG_CVT_JOBTYPE_TO_STR(jobType) {
        switch (jobType) {
            case JOB_AUTH_LOGIN:
                return 'JOB_AUTH_LOGIN';
            case JOB_AUTH_REGISTER:
                return 'JOB_AUTH_REGISTER';
            case JOB_USER_GET:
                return 'JOB_USER_GET';
            case JOB_USERS_INDEX:
                return 'JOB_USERS_INDEX';
            case JOB_USER_UPDATE:
                return 'JOB_USER_UPDATE';
            case JOB_USER_DESTROY:
                return 'JOB_USER_DESTROY';
            case JOB_CATEGORY_STORE:
                return "JOB_CATEGORY_STORE";
            case JOB_CATEGORIES_INDEX:
                return 'JOB_CATEGORIES_INDEX';
            case JOB_CATEGORY_UPDATE:
                return 'JOB_CATEGORY_UPDATE';
            case JOB_CATEGORY_DESTROY:
                return 'JOB_CATEGORY_DESTROY';
            case JOB_COUNTY_STORE:
                return 'JOB_COUNTY_STORE';
            case JOB_COUNTIES_INDEX:
                return 'JOB_COUNTIES_INDEX';
            case JOB_COUNTY_UPDATE:
                return 'JOB_COUNTY_UPDATE';
            case JOB_COUNTY_DESTROY:
                return 'JOB_COUNTY_DESTROY';
            case JOB_CLASSIFIED_STORE:
                return 'JOB_CLASSIFIED_STORE';
            case JOB_CLASSIFIEDS_INDEX:
                return 'JOB_CLASSIFIEDS_INDEX';
            case JOB_CLASSIFIED_UPDATE:
                return 'JOB_CLASSIFIED_UPDATE';
            case JOB_CLASSIFIED_DESTROY:
                return 'JOB_CLASSIFIED_DESTROY';
            default:
                return jobType;
        }
    }
}

/**************
 *    auth    *
 **************/
function initAuth() {
    auth = new Auth();
}

function Auth() {
    /****************
     *    init()    *
     ****************/
    const UserRole = {
        USER:  0,
        ADMIN: 1
    };

    var session;
    var cookie = new Cookie();


    /*****************
     *    methods    *
     *****************/
    this.isLoggedIn = function () {
        return !!session;
    };

    this.updateUserDetails = function (userPo) {
        if (objectHasProperty(userPo, 'username')) {
            session.user.setUsername(userPo.username);
        }

        if (objectHasProperty(userPo, 'email')) {
            session.user.setEmail(userPo.email);
        }

        if (objectHasProperty(userPo, 'county')) {
            session.user.setCounty(userPo.county);
        }
    };

    this.token = function () {
        return session.token;
    };

    this.user = function () {
        return session.user;
    };

    this.roles = function () {
        return UserRole;
    };

    this.roleObjs = function () {
        var objs = [];

        for (var roleKey in UserRole) {
            if (objectHasProperty(UserRole, roleKey)) {
                objs.push({
                    id: UserRole[roleKey],
                    name: roleIntToStr(UserRole[roleKey])
                });
            }
        }

        return objs;
    };

    this.tryAuthByCookie = function (callback) {
        if (!cookie.hasValue('id') || !cookie.hasValue('token')) {
            /*
             * relevant cookie data partially or fully absent. clean up data
             * that may remain and tell Ui to enter logged out state
             */
            logout();

            callback();
            return;
        }

        var id = cookie.getValue('id');
        var token = cookie.getValue('token');

        api.getUser(id, function (success, user) {
            if (!success) {
                logout();
            }
            else {
                login(token, user);
            }

            callback();
        });
    };

    this.login = login;
    this.logout = logout;
    this.roleIntToStr = roleIntToStr;


    /*******************
     *    functions    *
     *******************/
    function login(token, userPo) {
        createSession(token, userPo);
        ui.login();
    }

    function logout() {
        destroySession();
        ui.logout();
    }

    function createSession(token, userPo) {
        session = {};
        session.token = token;
        session.user = new User(userPo);

        makeCookie(token, userPo.id);
    }

    function makeCookie(token, id) {
        cookie.setValue('id', id);
        cookie.setValue('token', token);
    }

    function destroySession() {
        destroyCookie();

        session = undefined;
    }

    function destroyCookie() {
        cookie.unsetValue('id');
        cookie.unsetValue('token');
    }

    function roleIntToStr(roleInt) {
        switch (roleInt) {
            case UserRole.USER:
                return 'User';
            case UserRole.ADMIN:
                return 'Administrator';
            default:
                return 'Invalid role';
        }
    }
}

function Cookie() {
    /****************
     *    init()    *
     ****************/
    /*
     * parts based on
     * https://developer.mozilla.org/en-US/docs/Web/API/document/cookie
     */
    var defaults = {
        maxAgeSeconds: 60 * 60 * 24 * 365  // a year
    };

    /*****************
     *    methods    *
     *****************/
    this.setValue = function (key, value, age) {
        var _age = isDefined(age)? age: defaults.maxAgeSeconds;
        var keyEncoded = encodeURIComponent(key);
        var valueEncoded = encodeURIComponent(value);

        document.cookie = '%key%=%value%; max-age=%age%'
            .replace(/%key%/g, keyEncoded)
            .replace(/%value%/g, valueEncoded)
            .replace(/%age%/g, _age);
    };

    this.unsetValue = function (key) {
        var keyEncoded = encodeURIComponent(key);

        document.cookie = '%key%=; expires=Thu, 01 Jan 1970 00:00:00 UTC'
            .replace(/%key%/g, keyEncoded);
    };

    this.hasValue = function (key) {
        return getValue(key) !== '';
    };

    this.getValue = getValue;


    /*******************
     *    functions    *
     *******************/
    function getValue(key) {
        var regexTpl = /(?:(?:^|.*;\s*)%key%\s*\=\s*([^;]*).*$)|^.*$/;
        var regexStr = regexTpl.source.replace(/%key%/g, key);
        var regex = new RegExp(regexStr);

        return document.cookie.replace(regex, "$1");
    }
}

function User(data) {
    /****************
     *    init()    *
     ****************/
    var id = objectHasProperty(data, 'id')?
        parseInt(data.id) : undefined;
    var username = objectHasProperty(data, 'username')?
        data.username : undefined;
    var role = objectHasProperty(data, 'role')?
        parseInt(data.role) : undefined;

    var email = objectHasProperty(data, 'email')?
        data.email : undefined;
    var county = objectHasProperty(data, 'county')?
        data.county : undefined;


    /*****************
     *    methods    *
     *****************/
    this.id = function () {
        return id;
    };

    this.username = function () {
        return username;
    };

    this.email = function () {
        return email;
    };

    this.county = function () {
        return county;
    };

    this.isAdmin = function () {
        return role === auth.roles().ADMIN;
    };

    this.setUsername = function (_username) {
        username = _username;
    };

    this.setEmail = function (_email) {
        email = _email;
    };

    this.setCounty = function (_county) {
        county = _county;
    };
}