"""vand URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.2/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path, include
from django.conf import settings
from django.conf.urls.static import static

from .views import home


urlpatterns = [
    path('',                 home, name='home'),
    path('admin/',           admin.site.urls),
    path('api/auth/',        include('app_auth.api.urls', namespace='auth-api')),
    path('api/users/',       include('app_users.api.urls', namespace='users-api')),
    path('api/categories/',  include('app_categories.api.urls', namespace='categories-api')),
    path('api/counties/',    include('app_counties.api.urls', namespace='counties-api')),
    path('api/images/',      include('app_images.api.urls', namespace='images-api')),
    path('api/classifieds/', include('app_classifieds.api.urls', namespace='classifieds-api')),
]

urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
