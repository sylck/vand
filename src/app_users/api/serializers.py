from rest_framework import serializers

from app_counties.api.serializers import CountyModelSerializer
from ..models import CustomUser


class CustomUserModelSerializer(serializers.ModelSerializer):
    county = CountyModelSerializer()

    class Meta:
        model = CustomUser
        fields = [
            'id',
            'username',
            'email',
            'role',
            'county',
        ]
