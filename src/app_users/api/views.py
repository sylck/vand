import json

from django.db.models import Q

from rest_framework import generics, permissions
from rest_framework.authentication import TokenAuthentication
from rest_framework.exceptions import ValidationError, NotFound, PermissionDenied
from rest_framework.response import Response
from rest_framework.views import APIView

from vand.api.views import BaseManageView

from .serializers import CustomUserModelSerializer
from ..models import CustomUser as User, UserRole


class UserIndexAPIView(generics.ListAPIView):
    queryset = User.objects.all()
    serializer_class = CustomUserModelSerializer

    def get_queryset(self, *args, **kwargs):
        qs = self.queryset
        filters = Q()

        username = self.request.GET.get('q')
        email = self.request.GET.get('email')
        exact = self.request.GET.get('exact')
        except_id = self.request.GET.get('except')

        try:
            exact = json.loads(exact.lower())
        except Exception:
            exact = False

        try:
            except_id = int(except_id)
        except Exception:
            except_id = None

        if username is not None:
            if exact:
                filters &= Q(username__iexact=username)
            else:
                filters &= Q(username__icontains=username)

        if email is not None:
            if exact:
                filters &= Q(email__iexact=email)
            else:
                filters &= Q(email__icontains=email)

        if except_id is not None:
            filters &= ~Q(pk=except_id)

        qs = qs.filter(filters)

        return qs


class UserGetAPIView(generics.RetrieveAPIView):
    queryset = User.objects.all()
    serializer_class = CustomUserModelSerializer


class UserUpdateAPIView(APIView):
    authentication_classes = [TokenAuthentication, ]
    permission_classes = [permissions.IsAuthenticated, ]

    def put(self, request, pk, format=None):
        user_id = pk

        username = request.POST.get('username')
        email = request.POST.get('email')
        password = request.POST.get('password')
        county_id = request.POST.get('county')
        role_id = request.POST.get('role')

        user_objs = User.objects.filter(pk=user_id)

        if not user_objs.exists():
            raise NotFound(detail='User with given ID does not exist.')

        user = user_objs.first()

        if request.user != user and request.user.role != UserRole.Admin.value:
            raise PermissionDenied(detail='Must be administrator to edit other users\' details.')

        required_values = [username, email, county_id]

        role_id_required = request.user != user and request.user.role == UserRole.Admin.value
        if role_id_required:
            required_values.append(role_id)

        if not all(required_values):
            raise ValidationError(detail='Incomplete data provided.')

        if role_id_required:
            try:
                role_id = int(role_id)
            except Exception:
                raise ValidationError(detail='Role ID not an integer value.')

            if role_id not in [role[0] for role in UserRole.choices()]:
                raise ValidationError(detail='Role ID value out of bounds.')

            user.role = role_id

        user.username = username
        user.email = email

        if password is not None:
            user.set_password(password)

        user.county_id = county_id
        user.save()

        content = CustomUserModelSerializer(user).data
        return Response(content)


class UserDestroyAPIView(generics.DestroyAPIView):
    authentication_classes = [TokenAuthentication, ]
    permission_classes = [permissions.IsAuthenticated, ]
    queryset = User.objects.all()

    def perform_destroy(self, instance):
        if self.request.user.role != UserRole.Admin.value:
            raise PermissionDenied(detail='Must be administrator to perform this action.')

        instance.delete()


class UserGetUpdateDestroyAPIView(BaseManageView):
    VIEWS_BY_METHOD = {
        'GET': UserGetAPIView.as_view,
        'PUT': UserUpdateAPIView.as_view,
        'DELETE': UserDestroyAPIView.as_view,
    }
