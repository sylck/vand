from django.urls import path

from .views import (
    UserIndexAPIView,
    UserGetUpdateDestroyAPIView,
)


app_name = 'users'

urlpatterns = [
    path('',          UserIndexAPIView.as_view(),            name='index'),       # /api/users/
    path('<int:pk>/', UserGetUpdateDestroyAPIView.as_view(), name='get-update'),  # /api/users/42/
]
