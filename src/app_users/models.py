from django.db import models
from django.contrib.auth.models import AbstractUser, UserManager

from enum import Enum

from app_counties.models import County


class ChoiceEnum(Enum):
    @classmethod
    def choices(cls):
        choices = list()

        for item in cls:
            choices.append((item.value, item.name))

        return tuple(choices)

    def __str__(self):
        return self.name

    def __int__(self):
        return self.value


class UserRole(ChoiceEnum):
    User = 0
    Admin = 1


class CustomUserManager(UserManager):
    def create_superuser(self, username, email, password, **extra_fields):
        user = super(CustomUserManager, self).create_superuser(username, email, password, **extra_fields)
        user.role = UserRole.Admin.value
        user.save(using=self._db)
        return user


class CustomUser(AbstractUser):
    role = models.IntegerField(choices=UserRole.choices(), default=UserRole.User.value)
    county = models.ForeignKey(County, on_delete=models.SET_NULL, related_name='users', null=True)

    objects = CustomUserManager()
