from django.urls import path

from .views import (
    ImageStoreAPIView,
)


app_name = 'images'

urlpatterns = [
    path('', ImageStoreAPIView.as_view(), name='store'),  # /api/images/
]
