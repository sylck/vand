from rest_framework import permissions
from rest_framework.authentication import TokenAuthentication
from rest_framework.response import Response
from rest_framework.views import APIView

from .serializers import ImageModelSerializer
from ..models import Image


class ImageStoreAPIView(APIView):
    authentication_classes = [TokenAuthentication, ]
    permission_classes = [permissions.IsAuthenticated, ]

    def put(self, request, format=None):
        image_obj = request.FILES.get('image')

        image = Image.objects.create(image=image_obj)

        content = ImageModelSerializer(image).data
        return Response(content)
