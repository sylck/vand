from rest_framework import serializers

from ..models import Image


class ImageModelSerializer(serializers.ModelSerializer):
    url = serializers.SerializerMethodField()
    size = serializers.SerializerMethodField()

    class Meta:
        model = Image
        fields = [
            'id',
            'url',
            'size',
        ]

    def get_url(self, obj):
        return obj.image.url

    def get_size(self, obj):
        return obj.image.size
