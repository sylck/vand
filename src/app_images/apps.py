from django.apps import AppConfig


class AppImagesConfig(AppConfig):
    name = 'app_images'
