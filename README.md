# V&auml;nd

A web application designed for posting classified advertisements (classifieds)
with a name inspired by product names of a certain Scandinavian store selling
furniture. Has a responsive web interface tailored for desktop and mobile
browsers alike.

It is implemented as a single-page application (SPA) which uses jQuery to
dynamically update interface and send requests to Django back end using its
REST-based API. REST-based functionality is provided by Django REST framework.
Application interface is built on the Material Design for Bootstrap
(MDBootstrap) UI toolkit. Responsive and animated rearrangement of classifieds
grid in interface is done using Masonry.

## Screenshots

### Desktop

<img src="docs/noresults_desktop.png" width="320"
title="No results message, desktop browser"/>&nbsp;
<img src="docs/latest_desktop.png" width="320"
title="Latest classifieds list, desktop browser"/>&nbsp;
<img src="docs/details_desktop.png" width="320"
title="Dialog with details for a classified advertisement, desktop browser"/>

### Mobile

<img src="docs/noresults_mobile.png" height="500"
title="No results message, mobile browser"/>&nbsp;
<img src="docs/latest_mobile.png" height="500"
title="Latest classifieds list, mobile browser"/>&nbsp;
<img src="docs/details_mobile.png" height="500"
title="Dialog with details for a classified advertisement, mobile browser"/>

## Getting started

### Installation

1. Clone this repository and enter cloned directory.
```bash
git clone https://gitlab.com/sylck/vand.git
cd vand
```

2. Add execute permission to `setup.sh` and run it. In the setup process, an
administrator account will be created. Enter values of your choice for its
username, email address and password when prompt appears.
```bash
chmod +x setup.sh
./setup.sh
```

### Starting

Start the server by executing `run.sh`.
```bash
./run.sh
```

### Usage

Open `localhost:8000` in your browser. You should be greeted by the interface
shown in [_Screenshots_](#screenshots).
